SRG - Sistema de Registración y Graficación
=======

## Descripción

Software para la adquisición de datos, grafiación y exportación de los mismos para el banco de pruebas educativo de Tecnica Didactica S.A.

Los datos se adquieren a traves de una comunicación Serie por puerto COM. 

## Programación, librerias y compilación

El programa se encuentra escrito en VB.net, utilizando el Framework .

Es un software de escritorio basado en WinForms.