﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConexion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConexion))
        Me.cmbport = New System.Windows.Forms.ComboBox()
        Me.ConectarBtn = New System.Windows.Forms.Button()
        Me.ActualizaBtn = New System.Windows.Forms.Button()
        Me.CerrarBtn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cmbport
        '
        Me.cmbport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbport.FormattingEnabled = True
        Me.cmbport.Location = New System.Drawing.Point(12, 12)
        Me.cmbport.Name = "cmbport"
        Me.cmbport.Size = New System.Drawing.Size(147, 21)
        Me.cmbport.TabIndex = 0
        '
        'ConectarBtn
        '
        Me.ConectarBtn.Location = New System.Drawing.Point(176, 12)
        Me.ConectarBtn.Name = "ConectarBtn"
        Me.ConectarBtn.Size = New System.Drawing.Size(81, 29)
        Me.ConectarBtn.TabIndex = 1
        Me.ConectarBtn.Text = "Conectar"
        Me.ConectarBtn.UseVisualStyleBackColor = True
        '
        'ActualizaBtn
        '
        Me.ActualizaBtn.Location = New System.Drawing.Point(12, 53)
        Me.ActualizaBtn.Name = "ActualizaBtn"
        Me.ActualizaBtn.Size = New System.Drawing.Size(147, 21)
        Me.ActualizaBtn.TabIndex = 2
        Me.ActualizaBtn.Text = "Actualiza"
        Me.ActualizaBtn.UseVisualStyleBackColor = True
        '
        'CerrarBtn
        '
        Me.CerrarBtn.Location = New System.Drawing.Point(176, 47)
        Me.CerrarBtn.Name = "CerrarBtn"
        Me.CerrarBtn.Size = New System.Drawing.Size(81, 27)
        Me.CerrarBtn.TabIndex = 3
        Me.CerrarBtn.Text = "Cerrar"
        Me.CerrarBtn.UseVisualStyleBackColor = True
        '
        'frmConexion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(269, 116)
        Me.ControlBox = False
        Me.Controls.Add(Me.CerrarBtn)
        Me.Controls.Add(Me.ActualizaBtn)
        Me.Controls.Add(Me.ConectarBtn)
        Me.Controls.Add(Me.cmbport)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmConexion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Conexion"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmbport As System.Windows.Forms.ComboBox
    Friend WithEvents ConectarBtn As System.Windows.Forms.Button
    Friend WithEvents ActualizaBtn As System.Windows.Forms.Button
    Friend WithEvents CerrarBtn As System.Windows.Forms.Button
End Class
