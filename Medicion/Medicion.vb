﻿Public Class Medicion
    Private _Tiempo As String
    Private _Vca1 As String
    Private _Aca1 As String
    Private _Vca2 As String
    Private _Aca2 As String
    Private _PotenciaCA1 As String
    Private _PotenciaCA2 As String
    Private _PotenciaCC1 As String
    Private _PotenciaCC2 As String
    Private _CosenoCA1 As String
    Private _CosenoCA2 As String
    Private _Frecuencia As String
    Private _Vcc1 As String
    Private _Acc1 As String
    Private _Vcc2 As String
    Private _Acc2 As String
    Private _RPM As String
    Private _Torque As String
    Private _Medicion As Integer
    Public Property Tiempo() As Integer
        Get
            Return _Tiempo
        End Get
        Set(ByVal value As Integer)
            _Tiempo = value
        End Set
    End Property
    Public Property Vca1() As String
        Get
            Return _Vca1
        End Get
        Set(ByVal value As String)
            _Vca1 = value
        End Set
    End Property
    Public Property Vca2() As String
        Get
            Return _Vca2
        End Get
        Set(ByVal value As String)
            _Vca2 = value
        End Set
    End Property
    Public Property Vcc1() As String
        Get
            Return _Vcc1
        End Get
        Set(ByVal value As String)
            _Vcc1 = value
        End Set
    End Property
    Public Property Vcc2() As String
        Get
            Return _Vcc2
        End Get
        Set(ByVal value As String)
            _Vcc2 = value
        End Set
    End Property
    Public Property Aca1() As String
        Get
            Return _Aca1
        End Get
        Set(ByVal value As String)
            _Aca1 = value
        End Set
    End Property
    Public Property Aca2() As String
        Get
            Return _Aca2
        End Get
        Set(ByVal value As String)
            _Aca2 = value
        End Set
    End Property
    Public Property Acc1() As String
        Get
            Return _Acc1
        End Get
        Set(ByVal value As String)
            _Acc1 = value
        End Set
    End Property
    Public Property Acc2() As String
        Get
            Return _Acc2
        End Get
        Set(ByVal value As String)
            _Acc2 = value
        End Set
    End Property
    Public Property PotenciaCA1() As String
        Get
            Return _PotenciaCA1
        End Get
        Set(ByVal value As String)
            _PotenciaCA1 = value
        End Set
    End Property
    Public Property PotenciaCA2() As String
        Get
            Return _PotenciaCA2
        End Get
        Set(ByVal value As String)
            _PotenciaCA2 = value
        End Set
    End Property
    Public Property PotenciaCC1() As String
        Get
            Return _PotenciaCC1
        End Get
        Set(ByVal value As String)
            _PotenciaCC1 = value
        End Set
    End Property
    Public Property PotenciaCC2() As String
        Get
            Return _PotenciaCC2
        End Get
        Set(ByVal value As String)
            _PotenciaCC2 = value
        End Set
    End Property
    Public Property Frecuencia() As String
        Get
            Return _Frecuencia
        End Get
        Set(ByVal value As String)
            _Frecuencia = value
        End Set
    End Property
    Public Property CosenoCA1() As String
        Get
            Return _CosenoCA1
        End Get
        Set(ByVal value As String)
            _CosenoCA1 = value
        End Set
    End Property
    Public Property CosenoCA2() As String
        Get
            Return _CosenoCA2
        End Get
        Set(ByVal value As String)
            _CosenoCA2 = value
        End Set
    End Property
    Public Property RPM() As String
        Get
            Return _RPM
        End Get
        Set(ByVal value As String)
            _RPM = value
        End Set
    End Property
    Public Property Torque() As String
        Get
            Return _Torque
        End Get
        Set(ByVal value As String)
            _Torque = value
        End Set
    End Property
    Public Property Medicion() As Integer
        Get
            Return _Medicion
        End Get
        Set(ByVal value As Integer)
            _Medicion = value
        End Set
    End Property
End Class
