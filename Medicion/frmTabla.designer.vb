﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTabla
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTabla))
        Me.InstrumentosTab = New System.Windows.Forms.Label()
        Me.GraficoTab = New System.Windows.Forms.Label()
        Me.GrillaDatos = New System.Windows.Forms.DataGridView()
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CerrarPrincipal = New System.Windows.Forms.Label()
        Me.ImprimirBtn = New System.Windows.Forms.Label()
        Me.GuardarBtn = New System.Windows.Forms.Label()
        Me.StopTip = New System.Windows.Forms.Button()
        Me.MedirAhoraTip = New System.Windows.Forms.Button()
        Me.PausaTip = New System.Windows.Forms.Button()
        Me.MedirTip = New System.Windows.Forms.Button()
        Me.StopBtn = New System.Windows.Forms.Button()
        Me.MedirAhoraBtn = New System.Windows.Forms.Button()
        Me.PausaBtn = New System.Windows.Forms.Button()
        Me.MedirBtn = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.GrillaDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'InstrumentosTab
        '
        Me.InstrumentosTab.BackColor = System.Drawing.Color.Transparent
        Me.InstrumentosTab.Location = New System.Drawing.Point(104, 68)
        Me.InstrumentosTab.Name = "InstrumentosTab"
        Me.InstrumentosTab.Size = New System.Drawing.Size(149, 19)
        Me.InstrumentosTab.TabIndex = 50
        '
        'GraficoTab
        '
        Me.GraficoTab.BackColor = System.Drawing.Color.Transparent
        Me.GraficoTab.Location = New System.Drawing.Point(427, 68)
        Me.GraficoTab.Name = "GraficoTab"
        Me.GraficoTab.Size = New System.Drawing.Size(149, 19)
        Me.GraficoTab.TabIndex = 51
        '
        'GrillaDatos
        '
        Me.GrillaDatos.AllowUserToAddRows = False
        Me.GrillaDatos.AllowUserToDeleteRows = False
        Me.GrillaDatos.AllowUserToResizeColumns = False
        Me.GrillaDatos.AllowUserToResizeRows = False
        Me.GrillaDatos.BackgroundColor = System.Drawing.SystemColors.Control
        Me.GrillaDatos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.GrillaDatos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GrillaDatos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.GrillaDatos.ColumnHeadersHeight = 26
        Me.GrillaDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GrillaDatos.DefaultCellStyle = DataGridViewCellStyle2
        Me.GrillaDatos.Location = New System.Drawing.Point(97, 126)
        Me.GrillaDatos.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.GrillaDatos.MultiSelect = False
        Me.GrillaDatos.Name = "GrillaDatos"
        Me.GrillaDatos.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.NullValue = "0"
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GrillaDatos.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.GrillaDatos.RowHeadersVisible = False
        Me.GrillaDatos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrillaDatos.RowsDefaultCellStyle = DataGridViewCellStyle4
        Me.GrillaDatos.RowTemplate.ReadOnly = True
        Me.GrillaDatos.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GrillaDatos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.GrillaDatos.Size = New System.Drawing.Size(846, 352)
        Me.GrillaDatos.TabIndex = 54
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "Id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "Id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.IdDataGridViewTextBoxColumn.Visible = False
        '
        'CerrarPrincipal
        '
        Me.CerrarPrincipal.BackColor = System.Drawing.Color.Transparent
        Me.CerrarPrincipal.Location = New System.Drawing.Point(996, 3)
        Me.CerrarPrincipal.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CerrarPrincipal.Name = "CerrarPrincipal"
        Me.CerrarPrincipal.Size = New System.Drawing.Size(17, 15)
        Me.CerrarPrincipal.TabIndex = 57
        '
        'ImprimirBtn
        '
        Me.ImprimirBtn.BackColor = System.Drawing.Color.Transparent
        Me.ImprimirBtn.Location = New System.Drawing.Point(190, 505)
        Me.ImprimirBtn.Name = "ImprimirBtn"
        Me.ImprimirBtn.Size = New System.Drawing.Size(65, 19)
        Me.ImprimirBtn.TabIndex = 81
        '
        'GuardarBtn
        '
        Me.GuardarBtn.BackColor = System.Drawing.Color.Transparent
        Me.GuardarBtn.Location = New System.Drawing.Point(120, 505)
        Me.GuardarBtn.Name = "GuardarBtn"
        Me.GuardarBtn.Size = New System.Drawing.Size(65, 19)
        Me.GuardarBtn.TabIndex = 80
        '
        'StopTip
        '
        Me.StopTip.BackColor = System.Drawing.Color.Transparent
        Me.StopTip.BackgroundImage = CType(resources.GetObject("StopTip.BackgroundImage"), System.Drawing.Image)
        Me.StopTip.FlatAppearance.BorderSize = 0
        Me.StopTip.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.StopTip.Location = New System.Drawing.Point(853, 473)
        Me.StopTip.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.StopTip.Name = "StopTip"
        Me.StopTip.Size = New System.Drawing.Size(100, 30)
        Me.StopTip.TabIndex = 89
        Me.StopTip.UseVisualStyleBackColor = False
        Me.StopTip.Visible = False
        '
        'MedirAhoraTip
        '
        Me.MedirAhoraTip.BackColor = System.Drawing.Color.Transparent
        Me.MedirAhoraTip.BackgroundImage = CType(resources.GetObject("MedirAhoraTip.BackgroundImage"), System.Drawing.Image)
        Me.MedirAhoraTip.FlatAppearance.BorderSize = 0
        Me.MedirAhoraTip.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MedirAhoraTip.Location = New System.Drawing.Point(818, 473)
        Me.MedirAhoraTip.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MedirAhoraTip.Name = "MedirAhoraTip"
        Me.MedirAhoraTip.Size = New System.Drawing.Size(100, 30)
        Me.MedirAhoraTip.TabIndex = 88
        Me.MedirAhoraTip.UseVisualStyleBackColor = False
        Me.MedirAhoraTip.Visible = False
        '
        'PausaTip
        '
        Me.PausaTip.BackColor = System.Drawing.Color.Transparent
        Me.PausaTip.BackgroundImage = CType(resources.GetObject("PausaTip.BackgroundImage"), System.Drawing.Image)
        Me.PausaTip.FlatAppearance.BorderSize = 0
        Me.PausaTip.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.PausaTip.Location = New System.Drawing.Point(783, 473)
        Me.PausaTip.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PausaTip.Name = "PausaTip"
        Me.PausaTip.Size = New System.Drawing.Size(100, 30)
        Me.PausaTip.TabIndex = 87
        Me.PausaTip.UseVisualStyleBackColor = False
        Me.PausaTip.Visible = False
        '
        'MedirTip
        '
        Me.MedirTip.BackColor = System.Drawing.Color.Transparent
        Me.MedirTip.BackgroundImage = CType(resources.GetObject("MedirTip.BackgroundImage"), System.Drawing.Image)
        Me.MedirTip.FlatAppearance.BorderSize = 0
        Me.MedirTip.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MedirTip.Location = New System.Drawing.Point(748, 473)
        Me.MedirTip.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MedirTip.Name = "MedirTip"
        Me.MedirTip.Size = New System.Drawing.Size(100, 30)
        Me.MedirTip.TabIndex = 86
        Me.MedirTip.UseVisualStyleBackColor = False
        Me.MedirTip.Visible = False
        '
        'StopBtn
        '
        Me.StopBtn.BackColor = System.Drawing.Color.Transparent
        Me.StopBtn.BackgroundImage = Global.Medicion.My.Resources.Resources.boton_Detener_gris
        Me.StopBtn.Enabled = False
        Me.StopBtn.FlatAppearance.BorderSize = 0
        Me.StopBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.StopBtn.Location = New System.Drawing.Point(885, 499)
        Me.StopBtn.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.StopBtn.Name = "StopBtn"
        Me.StopBtn.Size = New System.Drawing.Size(37, 30)
        Me.StopBtn.TabIndex = 85
        Me.StopBtn.UseVisualStyleBackColor = False
        '
        'MedirAhoraBtn
        '
        Me.MedirAhoraBtn.BackColor = System.Drawing.Color.Transparent
        Me.MedirAhoraBtn.BackgroundImage = CType(resources.GetObject("MedirAhoraBtn.BackgroundImage"), System.Drawing.Image)
        Me.MedirAhoraBtn.Enabled = False
        Me.MedirAhoraBtn.FlatAppearance.BorderSize = 0
        Me.MedirAhoraBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MedirAhoraBtn.Location = New System.Drawing.Point(850, 499)
        Me.MedirAhoraBtn.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MedirAhoraBtn.Name = "MedirAhoraBtn"
        Me.MedirAhoraBtn.Size = New System.Drawing.Size(37, 30)
        Me.MedirAhoraBtn.TabIndex = 84
        Me.MedirAhoraBtn.UseVisualStyleBackColor = False
        '
        'PausaBtn
        '
        Me.PausaBtn.BackColor = System.Drawing.Color.Transparent
        Me.PausaBtn.BackgroundImage = CType(resources.GetObject("PausaBtn.BackgroundImage"), System.Drawing.Image)
        Me.PausaBtn.Enabled = False
        Me.PausaBtn.FlatAppearance.BorderSize = 0
        Me.PausaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.PausaBtn.Location = New System.Drawing.Point(812, 499)
        Me.PausaBtn.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PausaBtn.Name = "PausaBtn"
        Me.PausaBtn.Size = New System.Drawing.Size(37, 30)
        Me.PausaBtn.TabIndex = 83
        Me.PausaBtn.UseVisualStyleBackColor = False
        '
        'MedirBtn
        '
        Me.MedirBtn.BackColor = System.Drawing.Color.Transparent
        Me.MedirBtn.BackgroundImage = Global.Medicion.My.Resources.Resources.boton_Registrar_gris
        Me.MedirBtn.Enabled = False
        Me.MedirBtn.FlatAppearance.BorderSize = 0
        Me.MedirBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MedirBtn.Location = New System.Drawing.Point(780, 499)
        Me.MedirBtn.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MedirBtn.Name = "MedirBtn"
        Me.MedirBtn.Size = New System.Drawing.Size(37, 30)
        Me.MedirBtn.TabIndex = 82
        Me.MedirBtn.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.Medicion.My.Resources.Resources.numeros_tabla_transp
        Me.PictureBox1.Location = New System.Drawing.Point(97, 101)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(846, 25)
        Me.PictureBox1.TabIndex = 90
        Me.PictureBox1.TabStop = False
        '
        'frmTabla
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Medicion.My.Resources.Resources.base_tabla_16
        Me.ClientSize = New System.Drawing.Size(1022, 558)
        Me.ControlBox = False
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.StopTip)
        Me.Controls.Add(Me.MedirAhoraTip)
        Me.Controls.Add(Me.PausaTip)
        Me.Controls.Add(Me.MedirTip)
        Me.Controls.Add(Me.StopBtn)
        Me.Controls.Add(Me.MedirAhoraBtn)
        Me.Controls.Add(Me.PausaBtn)
        Me.Controls.Add(Me.MedirBtn)
        Me.Controls.Add(Me.ImprimirBtn)
        Me.Controls.Add(Me.GuardarBtn)
        Me.Controls.Add(Me.CerrarPrincipal)
        Me.Controls.Add(Me.GrillaDatos)
        Me.Controls.Add(Me.GraficoTab)
        Me.Controls.Add(Me.InstrumentosTab)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmTabla"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.GrillaDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents InstrumentosTab As System.Windows.Forms.Label
    Friend WithEvents GraficoTab As System.Windows.Forms.Label
    Friend WithEvents GrillaDatos As System.Windows.Forms.DataGridView
    Friend WithEvents IdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TsegDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vca1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Aca1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vca2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Aca2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CosDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FHzDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vcc1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Acc1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vcc2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Acc2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RPMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TqDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CerrarPrincipal As System.Windows.Forms.Label
    Friend WithEvents ImprimirBtn As System.Windows.Forms.Label
    Friend WithEvents GuardarBtn As System.Windows.Forms.Label
    Friend WithEvents StopTip As System.Windows.Forms.Button
    Friend WithEvents MedirAhoraTip As System.Windows.Forms.Button
    Friend WithEvents PausaTip As System.Windows.Forms.Button
    Friend WithEvents MedirTip As System.Windows.Forms.Button
    Friend WithEvents StopBtn As System.Windows.Forms.Button
    Friend WithEvents MedirAhoraBtn As System.Windows.Forms.Button
    Friend WithEvents PausaBtn As System.Windows.Forms.Button
    Friend WithEvents MedirBtn As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
End Class
