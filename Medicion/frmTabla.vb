﻿Imports System.Data
Imports System.Data.OleDb
Imports iTextSharp
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Public Class frmTabla
    Dim CantidadRegistros As Integer

    '------ EVENTOS DE FORMULARIOS ------

    Private Sub Tabla_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.SetStyle(ControlStyles.DoubleBuffer, True)
        StopBtn.Enabled = True
        MedirBtn.Enabled = True
        MedirAhoraBtn.Enabled = True
        PausaBtn.Enabled = True
        GraficoTab_Show()
        Try
            ActualizarTabla()
        Catch ex As Exception
            MsgBox("Se requiere la instalacion de un componente", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Error 101")
            Exit Sub
        End Try
    End Sub
    Private Sub formClose(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.FormClosed
        frmPrincipal.DesconectarPuerto()
    End Sub
    Private Sub GraficoTab_Show()
        ObtenerCantidadRegistros()
        If CantidadRegistros = 0 Then
            GraficoTab.Enabled = False
            GraficoTab.BackColor = Color.White
        Else
            GraficoTab.Enabled = True
            GraficoTab.BackColor = Color.Transparent
        End If
    End Sub

    Public Sub ActualizarTabla()
        Dim dbTabla As New dbTabla
        GrillaDatos.DataSource = dbTabla.SelectRegistros()
        Try
            Me.GrillaDatos.FirstDisplayedScrollingRowIndex = Me.GrillaDatos.RowCount - 1
        Catch
        End Try
        If CantidadRegistros = 0 Then
            GraficoTab_Show()
        End If
    End Sub
    Private Sub Tabla_VisibleChanged(sender As Object, e As System.EventArgs) Handles Me.VisibleChanged
        ActualizarTabla()
    End Sub
    Public Sub ObtenerCantidadRegistros()
        Dim Tabla As New dbTabla
        CantidadRegistros = Tabla.CountRegistros()
    End Sub

#Region "Configuracion DatagridView"
    Public Sub Configurardgv()
        'Columnas Ocultas
        GrillaDatos.Columns("Id").Visible = False
        GrillaDatos.Columns("Medicion").Visible = False

        ConfigurarColumna("Tiempo", "seg | N", 47, 1, -1)
        ConfigurarColumna("VCA1", "Vca", 47, 2, 0)
        ConfigurarColumna("Aca1", "Aca", 47, 3, 1)
        ConfigurarColumna("PotenciaCA1", "W", 47, 4, 2)
        ConfigurarColumna("CosenoCA1", "Cos Ø", 47, 5, 3)
        ConfigurarColumna("Vca2", "Vca", 47, 6, 4)
        ConfigurarColumna("Aca2", "Aca", 47, 7, 5)
        ConfigurarColumna("PotenciaCA2", "W", 47, 8, 6)
        ConfigurarColumna("CosenoCA2", "Cos Ø", 47, 9, 7)
        ConfigurarColumna("Vcc1", "Vcc", 47, 10, 8)
        ConfigurarColumna("Acc1", "Acc", 47, 11, 9)
        ConfigurarColumna("PotenciaCC1", "W", 47, 12, 10)
        ConfigurarColumna("Vcc2", "Vcc", 47, 13, 11)
        ConfigurarColumna("Acc2", "Acc", 47, 14, 12)
        ConfigurarColumna("PotenciaCC2", "W", 47, 15, 13)
        ConfigurarColumna("Frecuencia", "Hz", 47, 16, 14)
        ConfigurarColumna("RPM", "RPM", 47, 17, 16)
        ConfigurarColumna("Torque", "Tq", 47, 18, 15)

        GrillaDatos.SelectionMode = DataGridViewSelectionMode.FullRowSelect

    End Sub
    Private Sub ConfigurarColumna(Columna As String, Desc As String, Ancho As Integer, Indice As Integer, NBoton As Integer)
        GrillaDatos.Columns(Columna).HeaderText = Desc
        GrillaDatos.Columns(Columna).Width = Ancho
        GrillaDatos.Columns(Columna).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        GrillaDatos.Columns(Columna).DisplayIndex = Indice
        If NBoton <> -1 Then
            If frmPrincipal.Boton(NBoton) = True Then
                GrillaDatos.Columns(Columna).DefaultCellStyle.ForeColor = Color.Black
            Else
                GrillaDatos.Columns(Columna).DefaultCellStyle.ForeColor = Color.DarkGray
            End If
        End If
    End Sub
    Private Sub Datagridview_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles GrillaDatos.CellFormatting
        Select Case e.ColumnIndex
            Case 2
                e.CellStyle.BackColor = Color.LightGray
            Case 3
                e.CellStyle.BackColor = Color.LightGray
            Case 4
                e.CellStyle.BackColor = Color.LightGray
            Case 5
                e.CellStyle.BackColor = Color.LightGray
            Case 10
                e.CellStyle.BackColor = Color.LightGray
            Case 11
                e.CellStyle.BackColor = Color.LightGray
            Case 12
                e.CellStyle.BackColor = Color.LightGray
            Case 16
                e.CellStyle.BackColor = Color.LightGray
            Case 18
                e.CellStyle.BackColor = Color.LightGray
        End Select
    End Sub
#End Region

#Region "Botones"
    Private Sub MedirBtn_Click(sender As Object, e As EventArgs) Handles MedirBtn.Click
        frmPrincipal.MedirBtn.PerformClick()
    End Sub
    Private Sub PausaBtn_Click(sender As Object, e As EventArgs) Handles PausaBtn.Click
        frmPrincipal.PausaBtn.PerformClick()
    End Sub
    Private Sub MedirAhoraBtn_Click(sender As Object, e As EventArgs) Handles MedirAhoraBtn.Click
        frmPrincipal.MedirAhoraBtn.PerformClick()
        ActualizarTabla()
    End Sub
    Private Sub StopBtn_Click(sender As Object, e As EventArgs) Handles StopBtn.Click
        frmPrincipal.StopBtn.PerformClick()
    End Sub

    Private Sub GraficoTab_Click(sender As System.Object, e As System.EventArgs) Handles GraficoTab.Click
        frmGrafico.Show()
        Me.Hide()
    End Sub
    Private Sub InstrumentosTab_Click(sender As System.Object, e As System.EventArgs) Handles InstrumentosTab.Click
        frmPrincipal.Show()
        Me.Hide()
    End Sub

    Private Sub CerrarPrincipal_Click(sender As System.Object, e As System.EventArgs) Handles CerrarPrincipal.Click
        frmPrincipal.TiempoTmr.Enabled = False
        If MsgBox("¿Desea salir de la aplicacion?. Se perderan los datos que no hayan sido guardardos.", MsgBoxStyle.Question + MsgBoxStyle.OkCancel, "ID Instrumentos Digitales de Media Tensión") = vbOK Then
            frmPrincipal.VaciarBasedeDatos()
            frmPrincipal.DesconectarPuerto()
            Try
                frmGrafico.Close()
            Catch
            End Try
            Try
                frmConexion.Close()
            Catch
            End Try
            Try
                frmPrincipal.Close()
            Catch
            End Try
            Try
                SplashScreen.Close()
            Catch
            End Try
            Try
                Me.Close()
            Catch
            End Try
        Else
            frmPrincipal.TiempoTmr.Enabled = True
            Exit Sub
        End If
    End Sub

    Private Sub GuardarBtn_Click(sender As System.Object, e As System.EventArgs) Handles GuardarBtn.Click
        ObtenerCantidadRegistros()
        If CantidadRegistros = 0 Then
            MsgBox("Se requiere al menos un registro para exportar los datos.", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Error")
            Exit Sub
        End If
        frmPrincipal.TiempoTmr.Enabled = False
        MsgBox("Los Datos se exportarán a un archivo excel.", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Aviso Dataplex")

        Dim excel As New Microsoft.Office.Interop.Excel.Application
        Dim wBook As Microsoft.Office.Interop.Excel.Workbook
        Dim wSheet As Microsoft.Office.Interop.Excel.Worksheet

        wBook = excel.Workbooks.Add()
        wSheet = wBook.ActiveSheet()
        wSheet.Name = "ID"

        wSheet.Range("B1:B2").Merge()
        wSheet.Range("C1:S1").Merge()
        wSheet.Range("C2:S2").Merge()
        wSheet.Range("B3:S3").Merge()

        wSheet.Range("B1:B2").Font.Bold = True
        wSheet.Range("B1:B2").Font.Size = 26
        wSheet.Range("C1:S1").Font.Bold = True
        wSheet.Range("C2:S2").Font.Bold = True
        wSheet.Range("B3:S3").Font.Bold = True
        wSheet.Range("B5:O5").Font.Bold = True
        wSheet.Range("B3:S3").Merge()

        wBook.Worksheets("ID").cells(1, 2).value = "SRG"
        wBook.Worksheets("ID").cells(1, 3).value = "Sistema de registro"
        wBook.Worksheets("ID").cells(2, 3).value = "y graficación"
        wBook.Worksheets("ID").cells(3, 2).value = "ID - Instrumentos Digitales de Medición"

        wBook.Worksheets("ID").Columns("B:S").ColumnWidth = 10

        Dim Tabla As New dbTabla
        Dim dt As DataTable = Tabla.SelectRegistros
        Dim dc As System.Data.DataColumn
        Dim dr As System.Data.DataRow
        Dim colIndex As Integer = 0
        Dim rowIndex As Integer = 0

        'Agregamos los encabezados y numeros de instrumentos
        For Each dc In dt.Columns
            colIndex = colIndex + 1
            If colIndex > 0 And colIndex < 19 Then
                wBook.Worksheets("ID").cells(5, colIndex + 1).value = colIndex - 1
            End If
        Next

        wBook.Worksheets("ID").cells(6, 2).value = "Seg"
        wBook.Worksheets("ID").cells(6, 3).value = "Vca"
        wBook.Worksheets("ID").cells(6, 4).value = "Aca"
        wBook.Worksheets("ID").cells(6, 5).value = "W"
        wBook.Worksheets("ID").cells(6, 6).value = "Cos Ø"
        wBook.Worksheets("ID").cells(6, 7).value = "Vca"
        wBook.Worksheets("ID").cells(6, 8).value = "Aca"
        wBook.Worksheets("ID").cells(6, 9).value = "W"
        wBook.Worksheets("ID").cells(6, 10).value = "Cos Ø"
        wBook.Worksheets("ID").cells(6, 11).value = "Vcc"
        wBook.Worksheets("ID").cells(6, 12).value = "Acc"
        wBook.Worksheets("ID").cells(6, 13).value = "W"
        wBook.Worksheets("ID").cells(6, 14).value = "Vcc"
        wBook.Worksheets("ID").cells(6, 15).value = "Acc"
        wBook.Worksheets("ID").cells(6, 16).value = "W"
        wBook.Worksheets("ID").cells(6, 17).value = "Hz"
        wBook.Worksheets("ID").cells(6, 18).value = "Tq"
        wBook.Worksheets("ID").cells(6, 19).value = "RPM"

        rowIndex = rowIndex + 5

        For Each dr In dt.Rows
            rowIndex = rowIndex + 1
            colIndex = 0
            For Each dc In dt.Columns
                colIndex = colIndex + 1
                If colIndex > 0 And colIndex < 20 Then
                    excel.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                End If
            Next
        Next

        Dim blnFileOpen As Boolean = False
        Try
        Catch ex As Exception
            blnFileOpen = False
        End Try
        excel.Visible = True
    End Sub
    Private Sub ImprimirBtn_Click(sender As System.Object, e As System.EventArgs) Handles ImprimirBtn.Click
        ObtenerCantidadRegistros()
        If CantidadRegistros = 0 Then
            MsgBox("Se requiere al menos un registro para exportar los datos.", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Error")
            Exit Sub
        End If
        frmPrincipal.TiempoTmr.Enabled = False
        Try

            Dim documentoPDF As New Document(iTextSharp.text.PageSize.A4, 25, 10, 10, 10)
            Dim saveFileDialog1 As New SaveFileDialog()
            Dim writer As PdfWriter
            Dim Parrafo As Paragraph
            Dim Celda As PdfPCell
            Dim Fuente As iTextSharp.text.Font = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
            Dim FuenteNegrita As iTextSharp.text.Font = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
            Dim Limite As Integer
            Dim i, k As Integer
            Dim f_cn As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)

            Dim OldFile As String = String.Format("{0}\Plantilla.pdf", Application.StartupPath)

            'Create Reader
            Dim Reader As New PdfReader(OldFile)

            saveFileDialog1.Filter = "PDF Files (*.pdf)|*.pdf"
            saveFileDialog1.FilterIndex = 2
            saveFileDialog1.RestoreDirectory = True

            If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                writer = PdfWriter.GetInstance(documentoPDF, New FileStream(saveFileDialog1.FileName, FileMode.Create))
                MsgBox("El archivo PDF se guardo Satisfactoriamente en : " & saveFileDialog1.FileName, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Dataplex S.A.")
            End If

            documentoPDF.Open()
            'Cargamos los propiedades del archivo
            documentoPDF.AddAuthor("ID - Instrumentos Digitales de Media Tensión")
            documentoPDF.AddCreator("ID - Instrumentos Digitales de Media Tensión")
            documentoPDF.AddSubject("ID - Instrumentos Digitales de Media Tensión - Tabla")
            documentoPDF.AddTitle("ID - Instrumentos Digitales de Media Tensión - Tabla")
            documentoPDF.AddCreationDate()

            Dim cb As PdfContentByte = writer.DirectContent
            Dim table As New PdfPTable(18)
            Dim cell As New PdfPCell()
            Dim widths As Single() = New Single() {25, 28, 25, 25, 28, 28, 25, 25, 28, 28, 25, 28, 28, 25, 28, 25, 25, 25}

            table.TotalWidth = 474.0F
            cell.Colspan = 18

            table.DefaultCell.HorizontalAlignment = HorizontalAlignment.Right
            table.DefaultCell.VerticalAlignment = HorizontalAlignment.Center
            table.DefaultCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY

            table.AddCell(New Phrase("seg", FuenteNegrita))
            table.AddCell(New Phrase("Vca", FuenteNegrita))
            table.AddCell(New Phrase("Aca", FuenteNegrita))
            table.AddCell(New Phrase("W", FuenteNegrita))
            table.AddCell(New Phrase("Cos", FuenteNegrita))
            table.AddCell(New Phrase("Vca", FuenteNegrita))
            table.AddCell(New Phrase("Aca", FuenteNegrita))
            table.AddCell(New Phrase("W", FuenteNegrita))
            table.AddCell(New Phrase("Cos", FuenteNegrita))
            table.AddCell(New Phrase("Vcc", FuenteNegrita))
            table.AddCell(New Phrase("Acc", FuenteNegrita))
            table.AddCell(New Phrase("W", FuenteNegrita))
            table.AddCell(New Phrase("Vcc", FuenteNegrita))
            table.AddCell(New Phrase("Acc", FuenteNegrita))
            table.AddCell(New Phrase("W", FuenteNegrita))
            table.AddCell(New Phrase("Fhz", FuenteNegrita))
            table.AddCell(New Phrase("Tq", FuenteNegrita))
            table.AddCell(New Phrase("Rpm", FuenteNegrita))

            table.SetWidths(widths)

            i = 0
            Limite = CInt(GrillaDatos.RowCount.ToString) 'Obtengo el total de filas que(tiene)

            While i < Limite
                For k = 1 To 18
                    Parrafo = New Paragraph(String.Format(GrillaDatos.Rows(i).Cells(k).Value.ToString))
                    Parrafo.Font = Fuente
                    Parrafo.Font.Color = iTextSharp.text.BaseColor.GRAY
                    Parrafo.Alignment = Element.ALIGN_CENTER

                    Celda = New PdfPCell
                    If k Mod 2 = 0 Then 'Busca si la columna de la celda es par
                        Celda.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                    End If
                    Celda.AddElement(Parrafo)
                    Celda.HorizontalAlignment = HorizontalAlignment.Center
                    Celda.VerticalAlignment = HorizontalAlignment.Center
                    table.AddCell(Celda)
                Next
                i += 1
            End While

            Dim Paginas As Integer = Math.Floor(Limite / 33)
            Dim j As Integer = 0

            'If Paginas = 0 Then
            'Paginas = 1
            'End If

            Dim page As PdfImportedPage = writer.GetImportedPage(Reader, 1)

            For i = 0 To Paginas
                cb.AddTemplate(page, 0, 0)
                cb.MoveTo(80, 35)
                cb.LineTo(documentoPDF.PageSize.Width - 80, 35)
                cb.MoveTo(20, 35)
                cb.Stroke()
                cb.SetColorFill(BaseColor.GRAY)
                cb.BeginText()
                cb.SetFontAndSize(f_cn, 8)
                cb.ShowTextAligned(1, "Pagina " & i + 1 & " de " & Paginas + 1, documentoPDF.PageSize.Width - 100, 15, 0)
                cb.EndText()

                If i = 0 Then
                    table.WriteSelectedRows(j, j + 33, documentoPDF.Left + 60, documentoPDF.Top - 200, writer.DirectContent)
                    documentoPDF.NewPage()
                    j = j + 33
                Else
                    table.WriteSelectedRows(j, j + 33, documentoPDF.Left + 60, documentoPDF.Top - 200, writer.DirectContent)
                    documentoPDF.NewPage()
                    j = j + 33
                End If

            Next i

            'Cerramos el objeto documento, guardamos y creamos el PDF
            documentoPDF.Close()
            'Comprobamos si se ha creado el fichero PDF
            If System.IO.File.Exists(saveFileDialog1.FileName) Then
                If MsgBox("Datos Exportados correctamente " + "¿Desea abrir el fichero PDF resultante?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    'Abrimos el fichero PDF con la aplicación asociada
                    System.Diagnostics.Process.Start(saveFileDialog1.FileName)
                End If
            Else
                MsgBox("El fichero PDF no se ha generado, " + "compruebe que tiene permisos en la carpeta de destino.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            End If
        Catch ex As Exception
            MsgBox(String.Format("Se ha producido un error al intentar convertir el texto a PDF: {0}{0}{1}", vbCrLf, ex.Message), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        End Try
    End Sub
#End Region

#Region "Tooltip por Imagenes"
    Private Sub PausaBtn_MouseLeave(sender As Object, e As System.EventArgs) Handles PausaBtn.MouseLeave
        PausaTip.Visible = False
    End Sub
    Private Sub PausaBtn_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles PausaBtn.MouseMove
        PausaTip.Visible = True
    End Sub
    Private Sub StopBtn_MouseLeave(sender As Object, e As System.EventArgs) Handles StopBtn.MouseLeave
        StopTip.Visible = False
    End Sub
    Private Sub StopBtn_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles StopBtn.MouseMove
        StopTip.Visible = True
    End Sub
    Private Sub MedirAhoraBtn_MouseLeave(sender As Object, e As System.EventArgs) Handles MedirAhoraBtn.MouseLeave
        MedirAhoraTip.Visible = False
    End Sub
    Private Sub MedirAhoraBtn_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MedirAhoraBtn.MouseMove
        MedirAhoraTip.Visible = True
    End Sub
    Private Sub MedirBtn_MouseLeave(sender As Object, e As System.EventArgs) Handles MedirBtn.MouseLeave
        MedirTip.Visible = False
    End Sub
    Private Sub MedirBtn_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MedirBtn.MouseMove
        MedirTip.Visible = True
    End Sub
#End Region

End Class