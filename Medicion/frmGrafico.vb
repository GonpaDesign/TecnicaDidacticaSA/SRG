﻿Imports DevExpress.XtraCharts
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Public Class frmGrafico

    Dim Boton(16) As Integer '0=Vca1, 1=Aca1, 2=Vca2, 3=Aca2, 4=PotenciaCA1, 5=Cos1, 6=Frecuencia, 7=Vcc1, 8=Acc1, 9=Vcc2, 10=Acc2, 11=RPM, 12=Torque
    '0=Vca1, 1=Aca1, 2=Pot1, 3=Cos1, 4=Vca2, 5=Aca2, 6=Pot2, 7=Cos2, 8=Vcc1, 9=Acc1, 10=Pot3, 11=Vcc2, 12=Acc2, 13=Pot4, 14=Fhz, 15=Tq, 16=RPM
    Dim ValorAbscisas As Integer
    Dim Grafico As Boolean = False

    '------ EVENTOS DE FORMULARIO ------

    Private Sub Grafico_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.SetStyle(ControlStyles.DoubleBuffer, True)
        ValorAbscisas = 1

        Dim i As Integer = 0
        Do Until i = 12
            Boton(i) = 0
            i += 1
        Loop
        i = 0

        ' inicio de todas las series del grafico
        'GraficoCtrl.Series("VCA1").Points.Add(New SeriesPoint("0", "0"))
        'GraficoCtrl.Series("VCA1").Points.Add(New SeriesPoint("10", "50"))
    End Sub
    Private Sub formClose(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.FormClosed
        frmPrincipal.DesconectarPuerto()
    End Sub

#Region "BOTONES"


    Private Sub GuardarBtn_Click(sender As System.Object, e As System.EventArgs) Handles GuardarBtn.Click
        Dim saveFileDialog1 As New SaveFileDialog()
        saveFileDialog1.Filter = "Png Files (*.png)|*.png"
        saveFileDialog1.FileName = ""
        saveFileDialog1.FilterIndex = 2
        saveFileDialog1.RestoreDirectory = True
        If saveFileDialog1.ShowDialog() = DialogResult.OK Then
            GraficoCtrl.ExportToImage(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Png)
            MsgBox("El Grafico se guardo satisfactoriamente en : " & saveFileDialog1.FileName, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "ID Instrumentos Digitales de Media Tensión")
        End If
    End Sub
    Private Sub ImprimirBtn_Click(sender As System.Object, e As System.EventArgs) Handles ImprimirBtn.Click
        GraficoCtrl.ExportToImage(String.Format("{0}\Grafico.png", Application.StartupPath), System.Drawing.Imaging.ImageFormat.Png)
        System.Diagnostics.Process.Start(String.Format("{0}\GeneradorPDF.exe", Application.StartupPath))
    End Sub
    Private Sub CerrarPrincipal_Click(sender As System.Object, e As System.EventArgs) Handles CerrarPrincipal.Click
        frmPrincipal.TiempoTmr.Enabled = False
        If MsgBox("¿Desea salir de la aplicacion?. Se perderan los datos que no hayan sido guardardos.", MsgBoxStyle.Question + MsgBoxStyle.OkCancel, "ID Instrumentos Digitales de Media Tensión") = 1 Then
            frmPrincipal.VaciarBasedeDatos()
            frmPrincipal.DesconectarPuerto()
            Try
                frmConexion.Close()
            Catch
            End Try
            Try
                frmPrincipal.Close()
            Catch
            End Try
            Try
                frmPrincipal.Close()
            Catch
            End Try
            Try
                SplashScreen.Close()
            Catch
            End Try
            Try
                Me.Close()
            Catch
            End Try
        Else
            frmPrincipal.TiempoTmr.Enabled = True
            Exit Sub
        End If
    End Sub
#Region "Botones Pestaña"
    Private Sub TablaTab_Click(sender As System.Object, e As System.EventArgs) Handles TablaTab.Click
        frmTabla.Show()
        frmTabla.Configurardgv()
        Me.Hide()
    End Sub
    Private Sub InstrumentosTab_Click(sender As System.Object, e As System.EventArgs) Handles InstrumentosTab.Click
        frmPrincipal.Show()
        Me.Hide()
    End Sub
#End Region
#Region "Botones X"
    Private Sub TBtn_Click(sender As System.Object, e As System.EventArgs) Handles TBtn.Click
        BorrarBotonesX()
        TBtn.BackgroundImage = My.Resources.boton_X_negro
        ValorAbscisas = 1
        RedibujarSeries()
    End Sub

    Private Sub Vca1XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Vca1XBtn.Click
        BotonX(Vca1XBtn, 2)
    End Sub
    Private Sub Aca1XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Aca1XBtn.Click
        BotonX(Aca1XBtn, 3)
    End Sub
    Private Sub Pot1XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Pot1XBtn.Click
        BotonX(Pot1XBtn, 4)
    End Sub
    Private Sub Cos1XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Cos1XBtn.Click
        BotonX(Cos1XBtn, 5)
    End Sub

    Private Sub Vca2XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Vca2XBtn.Click
        BotonX(Vca2XBtn, 6)
    End Sub
    Private Sub Aca2XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Aca2XBtn.Click
        BotonX(Aca2XBtn, 7)
    End Sub
    Private Sub Pot2XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Pot2XBtn.Click
        BotonX(Pot2XBtn, 8)
    End Sub
    Private Sub Cos2XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Cos2XBtn.Click
        BotonX(Cos2XBtn, 9)
    End Sub

    Private Sub Vcc1XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Vcc1XBtn.Click
        BotonX(Vcc1XBtn, 10)
    End Sub
    Private Sub Acc1XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Acc1XBtn.Click
        BotonX(Acc1XBtn, 11)
    End Sub
    Private Sub Pot3XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Pot3XBtn.Click
        BotonX(Pot3XBtn, 12)
    End Sub

    Private Sub Vcc2XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Vcc2XBtn.Click
        BotonX(Vcc2XBtn, 13)
    End Sub
    Private Sub Acc2XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Acc2XBtn.Click
        BotonX(Acc2XBtn, 14)
    End Sub
    Private Sub Pot4XBtn_Click(sender As System.Object, e As System.EventArgs) Handles Pot4XBtn.Click
        BotonX(Pot4XBtn, 15)
    End Sub

    Private Sub FHzXBtn_Click(sender As System.Object, e As System.EventArgs) Handles FHzXBtn.Click
        BotonX(FHzXBtn, 16)
    End Sub
    Private Sub TqXBtn_Click(sender As System.Object, e As System.EventArgs) Handles TqXBtn.Click
        BotonX(TqXBtn, 17)
    End Sub
    Private Sub RPMXBtn_Click(sender As System.Object, e As System.EventArgs) Handles RPMXBtn.Click
        BotonX(RPMXBtn, 18)
    End Sub


    Private Sub BotonX(Boton As Object, Abscisa As Integer)
        BorrarBotonesX()
        Boton.BackgroundImage = My.Resources.boton_X_negro
        ValorAbscisas = Abscisa
        RedibujarSeries()
    End Sub
#End Region
#Region "Boton Y"
    Private Sub Vca1YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Vca1YBtn.Click
        Boton(0) = Boton(0) + 1
        If Boton(0) = 5 Then
            Boton(0) = 0
        End If
        CambioParametrosBotones("VCA1", 2, Boton(0), Vca1YBtn)
        RedibujarSeries()
    End Sub
    Private Sub Aca1YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Aca1YBtn.Click
        Boton(1) = Boton(1) + 1
        If Boton(1) = 5 Then
            Boton(1) = 0
        End If
        CambioParametrosBotonesx10("ACA1", 3, Boton(1), Aca1YBtn)
        RedibujarSeries()
    End Sub
    Private Sub Pot1YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Pot1YBtn.Click
        Boton(2) = Boton(2) + 1
        If Boton(2) = 5 Then
            Boton(2) = 0
        End If
        CambioParametrosBotones("WCA1", 4, Boton(2), Pot1YBtn)
        RedibujarSeries()
    End Sub
    Private Sub Cos1YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Cos1YBtn.Click
        Boton(3) = Boton(3) + 1
        If Boton(3) = 5 Then
            Boton(3) = 0
        End If
        CambioParametrosBotones("COS1", 5, Boton(3), Cos1YBtn)
        RedibujarSeries()
    End Sub

    Private Sub Vca2YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Vca2YBtn.Click
        Boton(4) = Boton(4) + 1
        If Boton(4) = 5 Then
            Boton(4) = 0
        End If
        CambioParametrosBotones("VCA2", 6, Boton(4), Vca2YBtn)
        RedibujarSeries()
    End Sub
    Private Sub Aca2YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Aca2YBtn.Click
        Boton(5) = Boton(5) + 1
        If Boton(5) = 5 Then
            Boton(5) = 0
        End If
        CambioParametrosBotonesx10("ACA2", 7, Boton(5), Aca2YBtn)
        RedibujarSeries()
    End Sub
    Private Sub Pot2YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Pot2YBtn.Click
        Boton(6) = Boton(6) + 1
        If Boton(6) = 5 Then
            Boton(6) = 0
        End If
        CambioParametrosBotones("WCA2", 8, Boton(6), Pot2YBtn)
        RedibujarSeries()
    End Sub
    Private Sub Cos2YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Cos2YBtn.Click
        Boton(7) = Boton(7) + 1
        If Boton(7) = 5 Then
            Boton(7) = 0
        End If
        CambioParametrosBotones("COS2", 9, Boton(7), Cos2YBtn)
        RedibujarSeries()
    End Sub

    Private Sub Vcc1YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Vcc1YBtn.Click
        Boton(8) = Boton(8) + 1
        If Boton(8) = 5 Then
            Boton(8) = 0
        End If
        CambioParametrosBotones("VCC1", 10, Boton(8), Vcc1YBtn)
        RedibujarSeries()
    End Sub
    Private Sub Acc1YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Acc1YBtn.Click
        Boton(9) = Boton(9) + 1
        If Boton(9) = 5 Then
            Boton(9) = 0
        End If
        CambioParametrosBotonesx10("ACC1", 11, Boton(9), Acc1YBtn)
        RedibujarSeries()
    End Sub
    Private Sub Pot3YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Pot3YBtn.Click
        Boton(10) = Boton(10) + 1
        If Boton(10) = 5 Then
            Boton(10) = 0
        End If
        CambioParametrosBotones("WCC1", 12, Boton(10), Pot3YBtn)
        RedibujarSeries()
    End Sub

    Private Sub Vcc2YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Vcc2YBtn.Click
        Boton(11) = Boton(11) + 1
        If Boton(11) = 5 Then
            Boton(11) = 0
        End If
        CambioParametrosBotones("VCC2", 13, Boton(11), Vcc2YBtn)
        RedibujarSeries()
    End Sub
    Private Sub Acc2YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Acc2YBtn.Click
        Boton(12) = Boton(12) + 1
        If Boton(12) = 5 Then
            Boton(12) = 0
        End If
        CambioParametrosBotonesx10("ACC2", 14, Boton(12), Acc2YBtn)
        RedibujarSeries()
    End Sub
    Private Sub Pot4YBtn_Click(sender As System.Object, e As System.EventArgs) Handles Pot4YBtn.Click
        Boton(13) = Boton(13) + 1
        If Boton(13) = 5 Then
            Boton(13) = 0
        End If
        CambioParametrosBotones("WCC2", 15, Boton(13), Pot4YBtn)
        RedibujarSeries()
    End Sub

    Private Sub FHzYBtn_Click(sender As System.Object, e As System.EventArgs) Handles FHzYBtn.Click
        Boton(14) = Boton(14) + 1
        If Boton(14) = 5 Then
            Boton(14) = 0
        End If
        CambioParametrosBotones("FHz", 16, Boton(14), FHzYBtn)
        RedibujarSeries()
    End Sub
    Private Sub TqYBtn_Click(sender As System.Object, e As System.EventArgs) Handles TqYBtn.Click
        Boton(15) = Boton(15) + 1
        If Boton(15) = 5 Then
            Boton(15) = 0
        End If
        CambioParametrosBotones("TQ", 17, Boton(15), TqYBtn)
        RedibujarSeries()
    End Sub
    Private Sub RPMYBtn_Click(sender As System.Object, e As System.EventArgs) Handles RPMYBtn.Click
        Boton(16) = Boton(16) + 1
        If Boton(16) = 5 Then
            Boton(16) = 0
        End If
        CambioParametrosBotones("RPM", 18, Boton(16), RPMYBtn)
        RedibujarSeries()
    End Sub
#End Region
#End Region

    Public Sub BorrarBotonesX()
        TBtn.BackgroundImage = My.Resources.boton_X_gris

        Vca1XBtn.BackgroundImage = My.Resources.boton_X_gris
        Aca1XBtn.BackgroundImage = My.Resources.boton_X_gris
        Pot1XBtn.BackgroundImage = My.Resources.boton_X_gris
        Cos1XBtn.BackgroundImage = My.Resources.boton_X_gris

        Vca2XBtn.BackgroundImage = My.Resources.boton_X_gris
        Aca2XBtn.BackgroundImage = My.Resources.boton_X_gris
        Pot2XBtn.BackgroundImage = My.Resources.boton_X_gris
        Cos2XBtn.BackgroundImage = My.Resources.boton_X_gris

        Vcc1XBtn.BackgroundImage = My.Resources.boton_X_gris
        Acc1XBtn.BackgroundImage = My.Resources.boton_X_gris
        Pot3XBtn.BackgroundImage = My.Resources.boton_X_gris

        Vcc2XBtn.BackgroundImage = My.Resources.boton_X_gris
        Acc2XBtn.BackgroundImage = My.Resources.boton_X_gris
        Pot4XBtn.BackgroundImage = My.Resources.boton_X_gris

        FHzXBtn.BackgroundImage = My.Resources.boton_X_gris
        TqXBtn.BackgroundImage = My.Resources.boton_X_gris
        RPMXBtn.BackgroundImage = My.Resources.boton_X_gris
    End Sub
    Public Sub CambioParametrosBotones(ByVal Serie As String, ByVal Columna As Integer, ByVal Boton As Integer, ByVal Elemento As Button)
        Try
            GraficoCtrl.Series(Serie).Points.Clear()
        Catch
        End Try

        Dim MSerie As Series = GraficoCtrl.GetSeriesByName(Serie)
        If Grafico = False Then
            If MSerie IsNot Nothing Then
                MSerie.ChangeView(ViewType.Line)
            End If
        Else
            If MSerie IsNot Nothing Then
                MSerie.ChangeView(ViewType.Point)
            End If
        End If

        If Boton <> 0 Then
            For i = 0 To frmTabla.GrillaDatos.RowCount - 1
                If frmTabla.GrillaDatos.Rows(i).Cells(Columna).Value.ToString = "-" Or frmTabla.GrillaDatos.Rows(i).Cells(ValorAbscisas).Value.ToString = "-" Then
                Else
                    GraficoCtrl.Series(Serie).Points.Add(New SeriesPoint(frmTabla.GrillaDatos.Rows(i).Cells(ValorAbscisas).Value, frmTabla.GrillaDatos.Rows(i).Cells(Columna).Value))
                End If
            Next
        End If
        Try
            Select Case Boton
                Case 0
                    Elemento.BackgroundImage = My.Resources.boton_Y_gris
                Case 1
                    Elemento.BackgroundImage = My.Resources.boton_Y_marron
                    GraficoCtrl.Series(Serie).View.Color = Color.FromArgb(97, 61, 25)
                Case 2
                    Elemento.BackgroundImage = My.Resources.boton_Y_rojo
                    GraficoCtrl.Series(Serie).View.Color = Color.FromArgb(200, 40, 50)
                Case 3
                    Elemento.BackgroundImage = My.Resources.boton_Y_amarillo
                    GraficoCtrl.Series(Serie).View.Color = Color.FromArgb(248, 154, 40)
                Case 4
                    Elemento.BackgroundImage = My.Resources.boton_Y_cyan
                    GraficoCtrl.Series(Serie).View.Color = Color.FromArgb(27, 126, 193)
            End Select
        Catch
        End Try
    End Sub
    Public Sub CambioParametrosBotonesx10(ByVal Serie As String, ByVal Columna As Integer, ByVal Boton As Integer, ByVal Elemento As Button)
        Try
            GraficoCtrl.Series(Serie).Points.Clear()
        Catch
        End Try

        Dim MSerie As Series = GraficoCtrl.GetSeriesByName(Serie)
        If Grafico = False Then
            If MSerie IsNot Nothing Then
                MSerie.ChangeView(ViewType.Line)
            End If
        Else
            If MSerie IsNot Nothing Then
                MSerie.ChangeView(ViewType.Point)
            End If
        End If

        If Boton <> 0 Then
            For i = 0 To frmTabla.GrillaDatos.RowCount - 1
                If frmTabla.GrillaDatos.Rows(i).Cells(Columna).Value.ToString = "-" Or frmTabla.GrillaDatos.Rows(i).Cells(ValorAbscisas).Value.ToString = "-" Then
                Else
                    GraficoCtrl.Series(Serie).Points.Add(New SeriesPoint(frmTabla.GrillaDatos.Rows(i).Cells(ValorAbscisas).Value, frmTabla.GrillaDatos.Rows(i).Cells(Columna).Value * 100))
                End If
            Next
        End If
        Select Case Boton
            Case 0
                Elemento.BackgroundImage = My.Resources.boton_Y_gris
            Case 1
                Elemento.BackgroundImage = My.Resources.boton_Y_marron
                GraficoCtrl.Series(Serie).View.Color = Color.FromArgb(97, 61, 25)
            Case 2
                Elemento.BackgroundImage = My.Resources.boton_Y_rojo
                GraficoCtrl.Series(Serie).View.Color = Color.FromArgb(200, 40, 50)
            Case 3
                Elemento.BackgroundImage = My.Resources.boton_Y_amarillo
                GraficoCtrl.Series(Serie).View.Color = Color.FromArgb(248, 154, 40)
            Case 4
                Elemento.BackgroundImage = My.Resources.boton_Y_cyan
                GraficoCtrl.Series(Serie).View.Color = Color.FromArgb(27, 126, 193)
        End Select
    End Sub
    Public Sub RedibujarSeries()
        CambioParametrosBotones("VCA1", 2, Boton(0), Vca1YBtn)
        CambioParametrosBotonesx10("ACA1", 3, Boton(1), Aca1YBtn)
        CambioParametrosBotones("WCA1", 4, Boton(2), Pot1YBtn)
        CambioParametrosBotones("COS1", 5, Boton(3), Cos1YBtn)

        CambioParametrosBotones("VCA2", 6, Boton(4), Vca2YBtn)
        CambioParametrosBotonesx10("ACA2", 7, Boton(5), Aca2YBtn)
        CambioParametrosBotones("WCA2", 8, Boton(6), Pot2YBtn)
        CambioParametrosBotones("COS2", 9, Boton(7), Cos2YBtn)

        CambioParametrosBotones("VCC1", 10, Boton(8), Vcc1YBtn)
        CambioParametrosBotonesx10("ACC1", 11, Boton(9), Acc1YBtn)
        CambioParametrosBotones("WCC1", 12, Boton(10), Pot3YBtn)

        CambioParametrosBotones("VCC2", 13, Boton(11), Vcc2YBtn)
        CambioParametrosBotonesx10("ACC2", 14, Boton(12), Acc2YBtn)
        CambioParametrosBotones("WCC2", 15, Boton(13), Pot4YBtn)

        CambioParametrosBotones("FHZ", 16, Boton(14), FHzYBtn)
        CambioParametrosBotones("TQ", 17, Boton(15), TqYBtn)
        CambioParametrosBotones("RPM", 18, Boton(16), RPMYBtn)
    End Sub

    Private Sub GrafChangebtn_Click(sender As Object, e As EventArgs) Handles GrafChangebtn.Click
        If Grafico = False Then
            Grafico = True
            GrafChangebtn.BackgroundImage = My.Resources.Linea
        Else
            Grafico = False
            GrafChangebtn.BackgroundImage = My.Resources.Puntos
        End If
        RedibujarSeries()
    End Sub
    Public Sub VaciarBotones()
        Dim i As Integer = 0
        Do Until i = 17
            Boton(i) = 0
            i += 1
        Loop
        Vca1YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Aca1YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Pot1YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Cos1YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Vca2YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Aca2YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Pot2YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Cos2YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Vcc1YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Acc1YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Pot3YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Vcc2YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Acc2YBtn.BackgroundImage = My.Resources.boton_Y_gris
        Pot4YBtn.BackgroundImage = My.Resources.boton_Y_gris
        FHzYBtn.BackgroundImage = My.Resources.boton_Y_gris
        TqYBtn.BackgroundImage = My.Resources.boton_Y_gris
        RPMYBtn.BackgroundImage = My.Resources.boton_Y_gris
        RedibujarSeries()
    End Sub
End Class