Imports System.Data
Imports System.Data.OleDb

Imports System.IO
Imports System.Diagnostics
Imports DevExpress.XtraCharts
Public Class frmPrincipal

    Public Boton(16) As Boolean '0=Vca1, 1=Aca1, 2=Pot1, 3=Cos1, 4=Vca2, 5=Aca2, 6=Pot2, 7=Cos2, 8=Vcc1, 9=Acc1, 10=Pot3, 11=Vcc2, 12=Acc2, 13=Pot4, 14=Fhz, 15=Tq, 16=RPM
    Private ArrCadena() As String '0=Vca1, 1=Aca1, 2=Vca2, 3=Aca2, 4=RPM, 5=Torque, 6=Vcc1, 7=Acc1, 8=Vcc2, 9=Acc2, 10=Frecuencia, 11=Wca1 , 12=Cos1 , 13=Wca2 , 14=Cos2
    Private Escala(11) As Integer '0=Vca1, 1=Aca1, 2=W1, 3=Vca4, 4=Aca2, 5=W2, 6=Vcc1, 7=Acc1, 8=W3, 9=Vcc2, 10=Acc2, 11=W4
    Private Divisor(5) As Double '0=Acc1, 1=Acc2, 2=Aca1, 3=Aca2, 4=Pot
    Private RPMColor(10) As System.Drawing.Color
    Dim Medicion As Integer = 1
    Dim Med As Integer = 0
    Public Tiempo As Integer = 0
    Public TiempoPulso As Integer = 0
    Dim myPort As Array  'COM Ports detected on the system will be stored here

    Delegate Sub SetTextCallback(ByVal [text] As String) 'Added to prevent threading errors during receiveing of data
    Dim RxString As String

#Region "Variables Para promediar datos"
    Private Vcc1(11) As Integer
    Private Acc1(11) As Integer
    Private Vcc2(11) As Integer
    Private Acc2(11) As Integer
    Private Vca1(11) As Double
    Private Vca2(11) As Double
#End Region

    Private Vcc1i As Double = 0
    Private Acc1i As Double = 0
    Private Vcc2i As Double = 0
    Private Acc2i As Double = 0
    Private Vca1i As Double = 0
    Private Vca2i As Double = 0

#Region "Eventos de Formularios"
    Private Sub frmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SerialPort1.Close()
        Me.SetStyle(ControlStyles.DoubleBuffer, True)
        '*********************************

        Divisor(0) = 100
        Divisor(1) = 100
        Divisor(2) = 100
        Divisor(3) = 100
        Divisor(4) = 100

        Dim i As Integer = 0
        Do Until i = 10
            Vcc1(i) = 0
            Acc1(i) = 0
            Vcc2(i) = 0
            Acc2(i) = 0
            i = i + 1
        Loop

        BotonesInstState(False)
        conexionBtn.Enabled = True
        conexionBtn.Select()
        GraficoTab.Enabled = False
        GraficoTab.BackColor = Color.White
        TablaTab.Enabled = False
        frmTabla.GraficoTab.Enabled = False
        TablaTab.BackColor = Color.White
        btnAyuda.Enabled = True
    End Sub
    Private Sub formClose(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.FormClosed
        DesconectarPuerto()
    End Sub
#End Region

#Region "Eventos de Botones"
    Private Sub CerrarPrincipal_Click(sender As System.Object, e As System.EventArgs) Handles CerrarPrincipal.Click
        'TiempoTmr.Enabled = False
        If MsgBox("�Desea salir de la aplicacion?. Se perderan los datos que no hayan sido guardardos.", MsgBoxStyle.Question + MsgBoxStyle.OkCancel, "ID Instrumentos Digitales de Media Tensi�n") = 1 Then
            InstrumentosOff()
            Try
                DesconectarPuerto()
                VaciarBasedeDatos()
                Try
                    frmGrafico.Close()
                Catch
                End Try
                Try
                    frmTabla.Close()
                Catch
                End Try
                Try
                    frmConexion.Close()
                Catch
                End Try
                Try
                    SplashScreen.Close()
                Catch
                End Try
                Try
                    Me.Close()
                Catch
                End Try
            Catch
            End Try
        Else
            'TiempoTmr.Enabled = True
            Exit Sub
        End If
    End Sub

#Region "Botones de Medicion"
    Public Sub MedirBtn_Click(sender As System.Object, e As System.EventArgs) Handles MedirBtn.Click
        TiempoTmr.Enabled = True
        GraficoTab.Enabled = True
        GraficoTab.BackColor = Color.Transparent
        frmTabla.GraficoTab.Enabled = True
        frmTabla.GraficoTab.BackColor = Color.Transparent
        MedirAhoraBtn.Enabled = False
        MedirAhoraBtn.BackgroundImage = My.Resources.boton_Registrar_ahora_gris_claro
        frmTabla.MedirAhoraBtn.Enabled = False
        frmTabla.MedirAhoraBtn.BackgroundImage = My.Resources.boton_Registrar_ahora_gris_claro
        Med = 1
    End Sub
    Public Sub MedirAhoraBtn_Click(sender As System.Object, e As System.EventArgs) Handles MedirAhoraBtn.Click
        InsertarTodosLosValores()
        GraficoTab.Enabled = True
        GraficoTab.BackColor = Color.Transparent
        frmTabla.GraficoTab.Enabled = True
        frmTabla.GraficoTab.BackColor = Color.Transparent
        MedirBtn.Enabled = False
        MedirBtn.BackgroundImage = My.Resources.boton_Registrar_gris_claro
        frmTabla.MedirBtn.Enabled = False
        frmTabla.MedirBtn.BackgroundImage = My.Resources.boton_Registrar_gris_claro
        TiempoPulso += 1
    End Sub
    Public Sub PausaBtn_Click(sender As System.Object, e As System.EventArgs) Handles PausaBtn.Click
        If Med = 1 Then
            Med = -1
        ElseIf Med = -1 Then
            Med = 1
        End If
    End Sub
    Public Sub StopBtn_Click(sender As System.Object, e As System.EventArgs) Handles StopBtn.Click
        TiempoTmr.Interval = 1000
        TiempoTmr.Enabled = False
        Tiempo = 0

        ' CUANDO HACE STOP EL SISTEMA ARMA EL GRAFICO

        ' Datos en los graficos

        'Grafico.GraficoCtrl.Series("Serie1").Points.Add(New SeriesPoint(PromedioLbl.Text, DistanciaTxt.Text))

        ' Refresca el Grafico
        frmGrafico.GraficoCtrl.Invalidate()
        frmGrafico.GraficoCtrl.RefreshData()
        frmGrafico.GraficoCtrl.Update()
        frmGrafico.GraficoCtrl.Refresh()
    End Sub
#End Region

#Region "Botones de Instrumentos"
    Private Sub Vca1Btn_Click(sender As System.Object, e As System.EventArgs) Handles Vca1Btn.Click
        If Boton(0) = False Then
            Boton(0) = True
            Vca1Btn.BackgroundImage = My.Resources.boton_Vca_verde
            EnviarDatos("A")
        Else
            Boton(0) = False
            Vca1Btn.BackgroundImage = My.Resources.boton_Vca_gris
            Inst1Rango1.Value = 0
            EnviarDatos("a")
        End If
        InstrumentoConf(Boton(0), Inst1Digital)
        InstrumentoBack(Boton(0), Instrumento1, 1)
    End Sub
    Private Sub Aca1Btn_Click(sender As System.Object, e As System.EventArgs) Handles Aca1Btn.Click
        If Boton(1) = False Then
            Boton(1) = True
            Aca1Btn.BackgroundImage = My.Resources.boton_Aca_verde
            EnviarDatos("B")
        Else
            Boton(1) = False
            Aca1Btn.BackgroundImage = My.Resources.boton_Aca_gris
            Inst2Rango1.Value = 0
            EnviarDatos("b")
        End If
        InstrumentoConf(Boton(1), Inst2Digital)
        InstrumentoBack(Boton(1), Instrumento2, 2)
    End Sub
    Private Sub Watt1Btn_Click(sender As System.Object, e As System.EventArgs) Handles Watt1Btn.Click
        If Boton(2) = False Then
            Boton(2) = True
            Watt1Btn.BackgroundImage = My.Resources.boton_W_verde
        Else
            Boton(2) = False
            Watt1Btn.BackgroundImage = My.Resources.boton_W_gris
        End If
        InstrumentoConf(Boton(2), Inst3Digital)
        InstrumentoBack(Boton(2), Instrumento3, 3)
    End Sub
    Private Sub Cos1Btn_Click(sender As System.Object, e As System.EventArgs) Handles Cos1Btn.Click
        If Boton(3) = False Then
            Boton(3) = True
            Cos1Btn.BackgroundImage = My.Resources.boton_Cos_verde
        Else
            Boton(3) = False
            Cos1Btn.BackgroundImage = My.Resources.boton_Cos_gris
        End If
        InstrumentoConf(Boton(3), Inst4Digital)
    End Sub

    Private Sub Vca2Btn_Click(sender As System.Object, e As System.EventArgs) Handles Vca2Btn.Click
        If Boton(4) = False Then
            Boton(4) = True
            Vca2Btn.BackgroundImage = My.Resources.boton_Vca_verde
            EnviarDatos("C")
        Else
            Boton(4) = False
            Vca2Btn.BackgroundImage = My.Resources.boton_Vca_gris
            EnviarDatos("c")
        End If
        InstrumentoConf(Boton(4), Inst5Digital)
        InstrumentoBack(Boton(4), Instrumento5, 5)
    End Sub
    Private Sub Aca2Btn_Click(sender As System.Object, e As System.EventArgs) Handles Aca2Btn.Click
        If Boton(5) = False Then
            Boton(5) = True
            Aca2Btn.BackgroundImage = My.Resources.boton_Aca_verde
            EnviarDatos("D")
        Else
            Boton(5) = False
            Aca2Btn.BackgroundImage = My.Resources.boton_Aca_gris
            Inst4Rango1.Value = 0
            EnviarDatos("d")
        End If
        InstrumentoConf(Boton(5), Inst6Digital)
        InstrumentoBack(Boton(5), Instrumento6, 6)
    End Sub
    Private Sub Watt2Btn_Click(sender As System.Object, e As System.EventArgs) Handles Watt2Btn.Click
        If Boton(6) = False Then
            Boton(6) = True
            Watt2Btn.BackgroundImage = My.Resources.boton_W_verde
        Else
            Boton(6) = False
            Watt2Btn.BackgroundImage = My.Resources.boton_W_gris
        End If
        InstrumentoConf(Boton(6), Inst7Digital)
        InstrumentoBack(Boton(6), Instrumento7, 7)
    End Sub
    Private Sub Cos2Btn_Click(sender As System.Object, e As System.EventArgs) Handles Cos2Btn.Click
        If Boton(7) = False Then
            Boton(7) = True
            Cos2Btn.BackgroundImage = My.Resources.boton_Cos_verde
        Else
            Boton(7) = False
            Cos2Btn.BackgroundImage = My.Resources.boton_Cos_gris
        End If
        InstrumentoConf(Boton(7), Inst8Digital)
    End Sub

    Private Sub Vcc1Btn_Click(sender As System.Object, e As System.EventArgs) Handles Vcc1Btn.Click
        If Boton(8) = False Then
            Boton(8) = True
            Vcc1Btn.BackgroundImage = My.Resources.boton_Vcc_verde
            EnviarDatos("1")
            EnviarDatos("F")
        Else
            Boton(8) = False
            Vcc1Btn.BackgroundImage = My.Resources.boton_Vcc_gris
            Inst8Rango1.Value = 0
            EnviarDatos("2") 'Desactiva el rele del voltimetro 1
            EnviarDatos("f")
        End If
        InstrumentoConf(Boton(8), Inst9Digital)
        InstrumentoBack(Boton(8), Instrumento9, 9)
    End Sub
    Private Sub Acc1Btn_Click(sender As System.Object, e As System.EventArgs) Handles Acc1Btn.Click
        If Boton(9) = False Then
            Boton(9) = True
            Acc1Btn.BackgroundImage = My.Resources.boton_Acc_verde
            EnviarDatos("G")
        Else
            Boton(9) = False
            Acc1Btn.BackgroundImage = My.Resources.boton_Acc_gris
            Inst9Rango1.Value = 0
            EnviarDatos("g")
        End If
        InstrumentoConf(Boton(9), Inst10Digital)
        InstrumentoBack(Boton(9), Instrumento10, 10)
    End Sub
    Private Sub Watt3Btn_Click(sender As System.Object, e As System.EventArgs) Handles Watt3Btn.Click
        If Boton(10) = False Then
            Boton(10) = True
            Watt3Btn.BackgroundImage = My.Resources.boton_W_verde
        Else
            Boton(10) = False
            Watt3Btn.BackgroundImage = My.Resources.boton_W_gris
        End If
        InstrumentoConf(Boton(10), Inst11Digital)
        InstrumentoBack(Boton(10), Instrumento11, 11)
    End Sub

    Private Sub Vcc2Btn_Click(sender As System.Object, e As System.EventArgs) Handles Vcc2Btn.Click
        If Boton(11) = False Then
            Boton(11) = True
            Vcc2Btn.BackgroundImage = My.Resources.boton_Vcc_verde
            EnviarDatos("3") 'Activa el rele del voltimetro 1
            EnviarDatos("H")
        Else
            Boton(11) = False
            Vcc2Btn.BackgroundImage = My.Resources.boton_Vcc_gris
            Inst10Rango1.Value = 0
            EnviarDatos("4") 'Desactiva el rele del voltimetro 1
            EnviarDatos("h")
        End If
        InstrumentoConf(Boton(11), Inst12Digital)
        InstrumentoBack(Boton(11), Instrumento12, 12)
    End Sub
    Private Sub Acc2Btn_Click(sender As System.Object, e As System.EventArgs) Handles Acc2Btn.Click
        If Boton(12) = False Then
            Boton(12) = True
            Acc2Btn.BackgroundImage = My.Resources.boton_Acc_verde
            EnviarDatos("I")
        Else
            Boton(12) = False
            Acc2Btn.BackgroundImage = My.Resources.boton_Acc_gris
            Inst11Rango1.Value = 0
            EnviarDatos("i")
        End If
        InstrumentoConf(Boton(12), Inst13Digital)
        InstrumentoBack(Boton(12), Instrumento13, 13)
    End Sub
    Private Sub Watt4Btn_Click(sender As System.Object, e As System.EventArgs) Handles Watt4Btn.Click
        If Boton(13) = False Then
            Boton(13) = True
            Watt4Btn.BackgroundImage = My.Resources.boton_W_verde
        Else
            Boton(13) = False
            Watt4Btn.BackgroundImage = My.Resources.boton_W_gris
        End If
        InstrumentoConf(Boton(13), Inst14Digital)
        InstrumentoBack(Boton(13), Instrumento14, 14)
    End Sub

    Private Sub FHzBtn_Click(sender As System.Object, e As System.EventArgs) Handles FHzBtn.Click
        If Boton(14) = False Then
            Boton(14) = True
            FHzBtn.BackgroundImage = My.Resources.boton_Hz_verde
            EnviarDatos("E")
        Else
            Boton(14) = False
            FHzBtn.BackgroundImage = My.Resources.boton_Hz_gris
            EnviarDatos("e")
        End If
        InstrumentoConf(Boton(14), Inst15Digital)
    End Sub
    Private Sub TqBtn_Click(sender As System.Object, e As System.EventArgs) Handles TqBtn.Click
        If Boton(15) = False Then
            Boton(15) = True
            TqBtn.BackgroundImage = My.Resources.boton_Tq_verde
            EnviarDatos("K")
        Else
            Boton(15) = False
            TqBtn.BackgroundImage = My.Resources.boton_Tq_gris
            EnviarDatos("k")
        End If
        InstrumentoConf(Boton(15), Inst16Digital)
    End Sub
    Private Sub RPMBtn_Click(sender As System.Object, e As System.EventArgs) Handles RPMBtn.Click
        If Boton(16) = False Then
            Boton(16) = True
            RPMBtn.BackgroundImage = My.Resources.boton_RPM_verde
            EnviarDatos("J")
        Else
            Boton(16) = False
            RPMBtn.BackgroundImage = My.Resources.boton_RPM_gris
            EnviarDatos("j")
        End If
        InstrumentoConf(Boton(16), Inst17Digital)

        Dim i As Integer = 0
        Do Until i = 10
            RPMColor(i) = Color.White
            i = i + 1
        Loop
    End Sub
#End Region

#Region "Tooltip por Imagenes"
    Private Sub ResetBtn_MouseLeave(sender As Object, e As System.EventArgs) Handles ResetBtn.MouseLeave
        ResetTip.Visible = False
    End Sub
    Private Sub ResetBtn_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles ResetBtn.MouseMove
        ResetTip.Visible = True
    End Sub
    Private Sub PausaBtn_MouseLeave(sender As Object, e As System.EventArgs) Handles PausaBtn.MouseLeave
        PausaTip.Visible = False
    End Sub
    Private Sub PausaBtn_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles PausaBtn.MouseMove
        PausaTip.Visible = True
    End Sub
    Private Sub StopBtn_MouseLeave(sender As Object, e As System.EventArgs) Handles StopBtn.MouseLeave
        StopTip.Visible = False
    End Sub
    Private Sub StopBtn_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles StopBtn.MouseMove
        StopTip.Visible = True
    End Sub
    Private Sub MedirAhoraBtn_MouseLeave(sender As Object, e As System.EventArgs) Handles MedirAhoraBtn.MouseLeave
        MedirAhoraTip.Visible = False
    End Sub
    Private Sub MedirAhoraBtn_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MedirAhoraBtn.MouseMove
        MedirAhoraTip.Visible = True
    End Sub
    Private Sub MedirBtn_MouseLeave(sender As Object, e As System.EventArgs) Handles MedirBtn.MouseLeave
        MedirTip.Visible = False
    End Sub
    Private Sub MedirBtn_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MedirBtn.MouseMove
        MedirTip.Visible = True
    End Sub
#End Region

    Private Sub GraficoTab_Click(sender As System.Object, e As System.EventArgs) Handles GraficoTab.Click
        frmGrafico.Show()
        Me.Hide()
    End Sub
    Private Sub TablaTab_Click(sender As System.Object, e As System.EventArgs) Handles TablaTab.Click
        frmTabla.Show()
        frmTabla.Configurardgv()
    End Sub
    Private Sub conexionBtn_Click(sender As System.Object, e As System.EventArgs) Handles conexionBtn.Click
        frmConexion.Show()
        frmConexion.ActualizaBtn.PerformClick()
    End Sub
    Private Sub ResetBtn_Click(sender As Object, e As EventArgs) Handles ResetBtn.Click
        TiempoTmr.Enabled = False
        If MsgBox("�Desea reiniciar la aplicacion?. Se perderan los datos que no hayan sido guardardos.", MsgBoxStyle.Question + MsgBoxStyle.OkCancel, "ID Instrumentos Digitales de Media Tensi�n") = 1 Then
            InstrumentosOff()
            Try
                VaciarBasedeDatos()
                frmGrafico.VaciarBotones()
            Catch ex As Exception
            End Try
            MedirAhoraBtn.Enabled = True
            MedirBtn.Enabled = True
            MedirAhoraBtn.BackgroundImage = My.Resources.boton_Registrar_ahora_gris
            MedirBtn.BackgroundImage = My.Resources.boton_Registrar_gris
            frmTabla.MedirAhoraBtn.Enabled = True
            frmTabla.MedirBtn.Enabled = True
            frmTabla.MedirAhoraBtn.BackgroundImage = My.Resources.boton_Registrar_ahora_gris
            frmTabla.MedirBtn.BackgroundImage = My.Resources.boton_Registrar_gris
            GraficoTab.Enabled = False
            GraficoTab.BackColor = Color.White
            frmTabla.GraficoTab.Enabled = False
            frmTabla.GraficoTab.BackColor = Color.White
        Else
            TiempoTmr.Enabled = True
            Exit Sub
        End If
    End Sub
#End Region

#Region "Manejo de Puerto Serie"
    Private Sub SerialPort1_DataReceived(sender As Object, e As System.IO.Ports.SerialDataReceivedEventArgs) Handles SerialPort1.DataReceived
        CheckForIllegalCrossThreadCalls = False
        Try
            ReceivedText(SerialPort1.ReadLine())    'Automatically called every time a data is received at the serialPort
        Catch
            MsgBox("�Atencion! Se ha desconectado del puerto", MsgBoxStyle.Information)
            DesconectarPuerto()
        End Try
    End Sub
    Private Sub ReceivedText(ByVal [text] As String)
        'compares the ID of the creating Thread to the ID of the calling Thread
        If Me.rtbReceived.InvokeRequired Then
            Dim x As New SetTextCallback(AddressOf ReceivedText)
            Me.Invoke(x, New Object() {(text)})
        Else
            RxString = [text]
            rtbReceived.Text = RxString
            CortarRegistros()
            CargarInstrumentos()
        End If
    End Sub
    Public Sub DesconectarPuerto()
        InstrumentosOff()
        SerialPort1.Close()
        BotonesInstState(False)
        frmConexion.ConectarBtn.Text = "Conectar"
    End Sub
    Public Sub EnviarDatos(Texto As String)
        Try
            SerialPort1.Write(Texto & vbCr)
        Catch ex As Exception
            'MsgBox("Error de Comunicacion, intente conectarse nuevamente con el puerto.", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "Error de Comunicacion")
            'DesconectarPuerto()
        End Try
    End Sub
#End Region

    Public Sub BotonesInstState(State As Boolean)
        Vca1Btn.Enabled = State
        Aca1Btn.Enabled = State
        Watt1Btn.Enabled = State
        Cos1Btn.Enabled = State

        Vca2Btn.Enabled = State
        Aca2Btn.Enabled = State
        Watt2Btn.Enabled = State
        Cos2Btn.Enabled = State

        Vcc1Btn.Enabled = State
        Acc1Btn.Enabled = State
        Watt3Btn.Enabled = State

        Vcc2Btn.Enabled = State
        Acc2Btn.Enabled = State
        Watt4Btn.Enabled = State

        FHzBtn.Enabled = State
        RPMBtn.Enabled = State
        TqBtn.Enabled = State

        MedirAhoraBtn.Enabled = State
        PausaBtn.Enabled = State
        StopBtn.Enabled = State
        MedirBtn.Enabled = State
        ResetBtn.Enabled = State
        If State = True Then
            MedirBtn.BackgroundImage = My.Resources.boton_Registrar_gris
            MedirAhoraBtn.BackgroundImage = My.Resources.boton_Registrar_ahora_gris
            PausaBtn.BackgroundImage = My.Resources.boton_Pausa_gris
            StopBtn.BackgroundImage = My.Resources.boton_Detener_gris
            ResetBtn.BackgroundImage = My.Resources.boton_Reset_gris
        Else
            MedirBtn.BackgroundImage = My.Resources.boton_Registrar_gris_claro
            MedirAhoraBtn.BackgroundImage = My.Resources.boton_Registrar_ahora_gris_claro
            PausaBtn.BackgroundImage = My.Resources.boton_Pausa_gris_claro
            StopBtn.BackgroundImage = My.Resources.boton_Detener_gris_claro
            ResetBtn.BackgroundImage = My.Resources.boton_Reset_gris_claro
        End If
    End Sub
    Public Sub VaciarBasedeDatos()
        Dim GestorBD As New dbTabla
        GestorBD.Delete()
    End Sub
    Private Sub CortarRegistros()
        ArrCadena = RxString.Split(";")
    End Sub
    Public Sub TiempoTmr_Tick(sender As System.Object, e As System.EventArgs) Handles TiempoTmr.Tick
        TiempoTmr.Interval = 1000
        Tiempo = Tiempo + 1
        If Med = 1 Then
            InsertarTodosLosValores()
            frmTabla.ActualizarTabla()
        End If
    End Sub
    Public Sub InsertarTodosLosValores()
        Try
            Dim UnaMedicion As New Medicion
            Dim GestorBD As New dbTabla

            If TiempoTmr.Enabled = True Then
                UnaMedicion.Tiempo = CStr(Tiempo)
            Else
                UnaMedicion.Tiempo = CStr(TiempoPulso)
            End If

            UnaMedicion.Vca1 = Inst1Digital.Text
            If CDbl(Inst2Digital.Text) > 5 Then
                UnaMedicion.Aca1 = CStr(CDbl(Inst2Digital.Text) / 1000)
            Else
                UnaMedicion.Aca1 = Inst2Digital.Text
            End If
            UnaMedicion.PotenciaCA1 = Inst3Digital.Text
            UnaMedicion.CosenoCA1 = Inst4Digital.Text

            UnaMedicion.Vca2 = Inst5Digital.Text
            If CDbl(Inst6Digital.Text) > 5 Then
                UnaMedicion.Aca2 = CStr(CDbl(Inst6Digital.Text) / 1000)
            Else
                UnaMedicion.Aca2 = Inst6Digital.Text
            End If
            UnaMedicion.PotenciaCA2 = Inst7Digital.Text
            UnaMedicion.CosenoCA2 = Inst8Digital.Text

            UnaMedicion.Vcc1 = Inst9Digital.Text
            If CDbl(Inst10Digital.Text) > 5 Then
                UnaMedicion.Acc1 = CStr(CDbl(Inst10Digital.Text) / 1000)
            Else
                UnaMedicion.Acc1 = Inst10Digital.Text
            End If
            UnaMedicion.PotenciaCC1 = Inst11Digital.Text

            UnaMedicion.Vcc2 = Inst12Digital.Text
            If CDbl(Inst13Digital.Text) > 5 Then
                UnaMedicion.Acc2 = CStr(CDbl(Inst13Digital.Text) / 1000)
            Else
                UnaMedicion.Acc2 = Inst13Digital.Text
            End If
            UnaMedicion.PotenciaCC2 = Inst14Digital.Text

            UnaMedicion.Frecuencia = Inst15Digital.Text
            UnaMedicion.Torque = Inst16Digital.Text
            UnaMedicion.RPM = Inst17Digital.Text

            UnaMedicion.Medicion = Medicion
            GestorBD.InsertarRegistros(UnaMedicion)
        Catch ex As Exception
            TiempoTmr.Enabled = False
            MsgBox("Verifique que la carpeta de instalacion de la aplicacion tenga los permisos de lectura/escritura habilitados", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Error")
        End Try
    End Sub
    Private Sub CargarInstrumentos()
        Try
            CargarGauge(Inst1Digital, Inst1Rango1, CDbl(ArrCadena(0)) / 100, 0) 'DIFERENECIA DE POTENCIAL 1- ALTERNA
            CargarGauge(Inst2Digital, Inst2Rango1, CDbl(ArrCadena(1)) / Divisor(2), 1) 'CORRIENTE 1- ALTERNA
            CargarGauge(Inst3Digital, Inst3Rango1, CDbl(Math.Abs(CInt(ArrCadena(11)) / 100)), 2) 'POTENCIA 1 - ALTERNA

            CargarGauge(Inst5Digital, Inst5Rango1, CDbl(ArrCadena(2)) / 100, 4) 'DIFERENECIA DE POTENCIAL 2- ALTERNA
            CargarGauge(Inst6Digital, Inst6Rango1, CDbl(ArrCadena(3)) / Divisor(3), 5) 'CORRIENTE 2- ALTERNA
            CargarGauge(Inst7Digital, Inst7Rango1, CDbl(Math.Abs(CInt(ArrCadena(13)) / 100)), 6) 'POTENCIA 2 - ALTERNA

            CargarGauge(Inst9Digital, Inst9Rango1, CDbl(ArrCadena(6)) / 100, 8) 'DIFERENCIA DE POTENCIAL 1- CONTINUA
            CargarGauge(Inst10Digital, Inst10Rango1, CInt(ArrCadena(7)) / Divisor(0), 9) 'CORRIENTE 1- CONTINUA
            CargarGauge(Inst11Digital, Inst11Rango1, (CDbl(Inst9Digital.Text) * (CInt(ArrCadena(7)) / 100)), 10) 'POTENCIAS 3 - CONTINUA

            CargarGauge(Inst12Digital, Inst12Rango1, CDbl(ArrCadena(8)) / 100, 11) 'DIFERENCIA DE POTENCIAL 2- CONTINUA
            CargarGauge(Inst13Digital, Inst13Rango1, CInt(ArrCadena(9)) / Divisor(1), 12) 'CORRIENTE 2- CONTINUA
            CargarGauge(Inst14Digital, Inst14Rango1, (CDbl(Inst12Digital.Text) * (CInt(ArrCadena(9)) / 100)), 13) 'POTENCIAS 4 - CONTINUA
        Catch
            MsgBox("Ha ocurrido un error en la recepcion de informaci�n", MsgBoxStyle.OkOnly)
            End Try

            If Acc1i < 0 Then
                Inst10Digital.ForeColor = Color.Red
            Else
                Inst10Digital.ForeColor = Color.Black
            End If
            If Acc2i < 0 Then
                Inst13Digital.ForeColor = Color.Red
            Else
                Inst13Digital.ForeColor = Color.Black
            End If

            'COSENO PHI 1
            If Boton(3) = True Then
                Inst4Digital.Text = Format(Math.Abs(CInt(ArrCadena(12)) / 100), "###0.00")
            Else
                Inst4Digital.Text = "0,00"
            End If
            'COSENO PHI 2
            If Boton(7) = True Then
                Inst8Digital.Text = Format(Math.Abs(CInt(ArrCadena(14)) / 100), "###0.00")
            Else
                Inst8Digital.Text = "0,00"
            End If

            'FRECUENCIA
            If Boton(14) = True Then
                Dim Fq As Double = CDbl(ArrCadena(10)) / 100
                Inst15Digital.Text = Format(Fq, "###0.00")
            Else
                Inst15Digital.Text = "0,00"
            End If

            'TORQUE
            If Boton(15) = True Then
                Dim Tq As Integer = Torque(ArrCadena(5))
                LinearScaleComponent7.Value = (Tq * 20)
                Inst16Digital.Text = Torque(ArrCadena(5))
            Else
                Inst16Digital.Text = "0,00"
                LinearScaleComponent7.Value = 0
            End If

            'RPM
            If Boton(16) = True Then
                Dim RPM As Integer = Revoluciones(ArrCadena(4))
                Inst17Digital.Text = RPM
                LinearScaleComponent8.Value = RPM / 32
            Else
                Inst17Digital.Text = "0,00"
                LinearScaleComponent8.Value = 0
            End If

            RxString = ""
    End Sub
    Private Sub CargarGauge(Instrumento As Object, Rango As Object, Valor As Double, NBoton As Integer)
        If Boton(NBoton) = True Then
            Select Case NBoton
                Case 0 'VCA1
                    If Valor < 3 Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = Instrumento.Text
                    Else
                        Instrumento.text = Format(Valor, "###0.00")
                        Rango.value = Instrumento.Text
                    End If
                Case 1 'ACA1
                    If Valor <= 0.05 Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = 0
                    Else
                        If Escala(1) = 2 Then
                            Instrumento.Text = Format(Valor * 1000, "###0.0")
                            Rango.Value = Instrumento.Text
                        Else
                            Instrumento.Text = Format(Valor, "###0.00")
                            Rango.Value = Instrumento.Text
                        End If
                    End If
                Case 2 'W1
                    If Inst1Digital.Text = "0,00" Or Inst2Digital.Text = "0,00" Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = 0
                    Else
                        Instrumento.Text = Format(Valor, "###0.00")
                        Rango.Value = Instrumento.Text
                    End If
                Case 4 'VCA2
                    If Valor < 3 Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = Instrumento.Text
                    Else
                        Instrumento.text = Format(Valor, "###0.00")
                        Rango.value = Instrumento.Text
                    End If
                Case 5 'ACA2
                    If Valor <= 0.05 Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = 0
                    Else
                        If Escala(4) = 2 Then
                            Instrumento.Text = Format(Valor * 1000, "###0.0")
                            Rango.Value = Instrumento.Text
                        Else
                            Instrumento.Text = Format(Valor, "###0.00")
                            Rango.Value = Instrumento.Text
                        End If
                    End If
                Case 6 'W2
                    If Inst5Digital.Text = "0,00" Or Inst6Digital.Text = "0,00" Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = 0
                    Else
                        Instrumento.Text = Format(Valor, "###0.00")
                        Rango.Value = Instrumento.Text
                    End If
                Case 8 'VCC1
                    If Valor < 3 Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = Instrumento.Text
                    Else
                        Instrumento.text = Format(Valor, "###0.00")
                        Rango.value = Instrumento.Text
                    End If
                Case 9 'ACC1
                    If Valor <= 0.05 Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = 0
                    Else
                        If Escala(7) = 2 Then
                            Instrumento.Text = Format(Valor * 1000, "###0.0")
                            Rango.Value = Instrumento.Text
                        Else
                            Instrumento.Text = Format(Valor, "###0.00")
                            Rango.Value = Instrumento.Text
                        End If
                    End If
                Case 10 'W3
                    If Inst9Digital.Text = "0,00" Or Inst10Digital.Text = "0,00" Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = 0
                    Else
                        Instrumento.Text = Format(Valor, "###0.00")
                        Rango.Value = Instrumento.Text
                    End If
                Case 11 'VCC2
                    If Valor < 3 Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = Instrumento.Text
                    Else
                        Instrumento.text = Format(Valor, "###0.00")
                        Rango.value = Instrumento.Text
                    End If
                Case 12 'ACC2
                    If Valor <= 0.05 Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = 0
                    Else
                        If Escala(10) = 2 Then
                            Instrumento.Text = Format(Valor * 1000, "###0.0")
                            Rango.Value = Instrumento.Text
                        Else
                            Instrumento.Text = Format(Valor, "###0.00")
                            Rango.Value = Instrumento.Text
                        End If
                    End If
                Case 13 'W4
                    If Inst12Digital.Text = "0,00" Or Inst13Digital.Text = "0,00" Then
                        Instrumento.text = Format(0, "###0.00")
                        Rango.value = 0
                    Else
                        Instrumento.Text = Format(Valor, "###0.00")
                        Rango.Value = Instrumento.Text
                    End If
            End Select
        Else
            Instrumento.Text = "0,00"
            Rango.Value = 0
        End If
    End Sub
    Private Sub Escalar(Rango As Object, Tipo As Integer, Escala As Integer)
        Select Case Tipo
            Case 1
                Select Case Escala
                    Case 2
                        Rango.MaxValue = 50
                        Rango.MajorTickCount = 6
                        Rango.MinorTickCount = 4
                    Case 1
                        Rango.MaxValue = 250
                        Rango.MajorTickCount = 6
                        Rango.MinorTickCount = 4
                    Case 0
                        Rango.MaxValue = 500
                        Rango.MajorTickCount = 6
                        Rango.MinorTickCount = 4
                End Select
            Case 2
                Select Case Escala
                    Case 2
                        Rango.MaxValue = 500
                        Rango.MajorTickCount = 6
                        Rango.MinorTickCount = 4
                        Rango.labels("Label0").Text = "mA"
                    Case 1
                        Rango.MaxValue = 2
                        Rango.MajorTickCount = 3
                        Rango.MinorTickCount = 9
                        Rango.labels("Label0").Text = "A"
                    Case 0
                        Rango.MaxValue = 5
                        Rango.MajorTickCount = 6
                        Rango.MinorTickCount = 4
                        Rango.labels("Label0").Text = "A"
                End Select
            Case 3
                Select Case Escala
                    Case 2
                        Rango.MaxValue = 100
                        Rango.MajorTickCount = 5
                        Rango.MinorTickCount = 4
                    Case 1
                        Rango.MaxValue = 500
                        Rango.MajorTickCount = 6
                        Rango.MinorTickCount = 4
                    Case 0
                        Rango.MaxValue = 2000
                        Rango.MajorTickCount = 5
                        Rango.MinorTickCount = 4
                End Select
        End Select
    End Sub
    Public Function Torque(Tq As String) As Double
        Torque = 0
        Torque = Math.Round(((Tq - 512) * (2.5 / 512)), 1)
        'Torque = Math.Round((CInt(Tq) / 100), 1)
        Return Torque
    End Function
    Public Function Revoluciones(Velocidad As String) As Double
        Revoluciones = 0
        Revoluciones = Math.Round((CInt(Velocidad) / 100), 0)
        Return Revoluciones
    End Function

#Region "Configuraciones"
    Public Sub OcultarInstrumentos()
        Instrumento1.Visible = False
        Instrumento2.Visible = False
        Instrumento3.Visible = False
        Instrumento6.Visible = False
        Instrumento9.Visible = False
        Instrumento10.Visible = False
        Instrumento12.Visible = False
        Instrumento13.Visible = False

        Inst1Digital.Visible = False
        Inst2Digital.Visible = False
        Inst3Digital.Visible = False
        Inst4Digital.Visible = False
        Inst5Digital.Visible = False
        Inst6Digital.Visible = False
        Inst7Digital.Visible = False
        Inst8Digital.Visible = False
        Inst9Digital.Visible = False
        Inst10Digital.Visible = False
        Inst11Digital.Visible = False
        Inst12Digital.Visible = False
        Inst13Digital.Visible = False
        Inst14Digital.Visible = False
        Inst15Digital.Visible = False
        Inst16Digital.Visible = False
        Inst17Digital.Visible = False

        Vca1Btn.Visible = False
        Aca1Btn.Visible = False
        Watt1Btn.Visible = False
        Vca2Btn.Visible = False
        Aca2Btn.Visible = False
        Watt2Btn.Visible = False
        Vcc1Btn.Visible = False
        Acc1Btn.Visible = False
        Watt3Btn.Visible = False
        Vcc2Btn.Visible = False
        Acc2Btn.Visible = False
        Watt4Btn.Visible = False
        Cos2Btn.Visible = False
        FHzBtn.Visible = False
        RPMBtn.Visible = False
        TqBtn.Visible = False

        ResetBtn.Visible = False
        StopBtn.Visible = False
        PausaBtn.Visible = False
        MedirAhoraBtn.Visible = False
        MedirBtn.Visible = False
    End Sub
    Public Sub MostrarInstrumentos()
        Instrumento1.Visible = True
        Instrumento2.Visible = True
        Instrumento3.Visible = True
        Instrumento6.Visible = True
        Instrumento9.Visible = True
        Instrumento10.Visible = True
        Instrumento12.Visible = True
        Instrumento13.Visible = True

        Inst1Digital.Visible = True
        Inst2Digital.Visible = True
        Inst3Digital.Visible = True
        Inst4Digital.Visible = True
        Inst5Digital.Visible = True
        Inst6Digital.Visible = True
        Inst7Digital.Visible = True
        Inst8Digital.Visible = True
        Inst9Digital.Visible = True
        Inst10Digital.Visible = True
        Inst11Digital.Visible = True
        Inst12Digital.Visible = True
        Inst13Digital.Visible = True
        Inst14Digital.Visible = True
        Inst15Digital.Visible = True
        Inst16Digital.Visible = True
        Inst17Digital.Visible = True

        Vca1Btn.Visible = True
        Aca1Btn.Visible = True
        Watt1Btn.Visible = True
        Vca2Btn.Visible = True
        Aca2Btn.Visible = True
        Watt2Btn.Visible = True
        Vcc1Btn.Visible = True
        Acc1Btn.Visible = True
        Watt3Btn.Visible = True
        Vcc2Btn.Visible = True
        Acc2Btn.Visible = True
        Watt4Btn.Visible = True
        Cos2Btn.Visible = True
        FHzBtn.Visible = True
        RPMBtn.Visible = True
        TqBtn.Visible = True

        ResetBtn.Visible = True
        StopBtn.Visible = True
        PausaBtn.Visible = True
        MedirAhoraBtn.Visible = True
        MedirBtn.Visible = True
    End Sub
    Public Sub InstrumentosOff()
        Dim i As Integer = 0
        Do Until i = 17
            Boton(i) = True
            i += 1
        Loop
        Try
            Vca1Btn.PerformClick()
            Aca1Btn.PerformClick()
            Watt1Btn.PerformClick()
            Cos1Btn.PerformClick()

            Vca2Btn.PerformClick()
            Aca2Btn.PerformClick()
            Watt2Btn.PerformClick()
            Cos2Btn.PerformClick()

            Vcc1Btn.PerformClick()
            Acc1Btn.PerformClick()
            Watt3Btn.PerformClick()

            Vcc2Btn.PerformClick()
            Acc2Btn.PerformClick()
            Watt4Btn.PerformClick()

            FHzBtn.PerformClick()
            TqBtn.PerformClick()
            RPMBtn.PerformClick()
        Catch
        End Try
    End Sub
    Public Sub InstrumentoConf(State As Boolean, Inst As Object)
        If State = True Then
            Inst.ForeColor = Color.Green
            Inst.Text = "0,00"
        Else
            Inst.ForeColor = Color.Black
            Inst.Text = "0,00"
        End If
    End Sub
    Public Sub InstrumentoBack(State As Boolean, Gauge As Object, Num As String)
        If State = True Then
            Select Case Num
                Case 1
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp1
                Case 2
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp2
                Case 3
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp3
                Case 5
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp5
                Case 6
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp6
                Case 7
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp7
                Case 9
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp9
                Case 10
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp10
                Case 11
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp11
                Case 12
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp12
                Case 13
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp13
                Case 14
                    Gauge.BackgroundImage = My.Resources.Reloj_verde_transp14
            End Select
        Else
            Gauge.BackgroundImage = Nothing
        End If
    End Sub
#End Region

#Region "Modificacion de escalas"
    Private Sub Instrumento1_Click(sender As Object, e As EventArgs) Handles Instrumento1.Click
        If Escala(0) = 2 Then
            Escala(0) = 0
        Else
            Escala(0) = Escala(0) + 1
        End If
        Escalar(Inst1Rango1, 1, Escala(0))
    End Sub
    Private Sub Instrumento2_Click(sender As Object, e As EventArgs) Handles Instrumento2.Click
        If Escala(1) = 2 Then
            Escala(1) = 0
        Else
            Escala(1) = Escala(1) + 1
        End If
        Escalar(Inst2Rango1, 2, Escala(1))
    End Sub
    Private Sub Instrumento3_Click(sender As Object, e As EventArgs) Handles Instrumento3.Click
        If Escala(2) = 2 Then
            Escala(2) = 0
        Else
            Escala(2) = Escala(2) + 1
        End If
        Escalar(Inst3Rango1, 3, Escala(2))
    End Sub
    Private Sub Instrumento5_Click(sender As Object, e As EventArgs) Handles Instrumento5.Click
        If Escala(3) = 2 Then
            Escala(3) = 0
        Else
            Escala(3) = Escala(3) + 1
        End If
        Escalar(Inst5Rango1, 1, Escala(3))
    End Sub
    Private Sub Instrumento6_Click(sender As Object, e As EventArgs) Handles Instrumento6.Click
        If Escala(4) = 2 Then
            Escala(4) = 0
        Else
            Escala(4) = Escala(4) + 1
        End If
        Escalar(Inst6Rango1, 2, Escala(4))
    End Sub
    Private Sub Instrumento7_Click(sender As Object, e As EventArgs) Handles Instrumento7.Click
        If Escala(5) = 2 Then
            Escala(5) = 0
        Else
            Escala(5) = Escala(5) + 1
        End If
        Escalar(Inst7Rango1, 3, Escala(5))
    End Sub
    Private Sub Instrumento9_Click(sender As Object, e As EventArgs) Handles Instrumento9.Click
        If Escala(6) = 2 Then
            Escala(6) = 0
        Else
            Escala(6) = Escala(6) + 1
        End If
        Escalar(Inst9Rango1, 1, Escala(6))
    End Sub
    Private Sub Instrumento10_Click(sender As Object, e As EventArgs) Handles Instrumento10.Click
        If Escala(7) = 2 Then
            Escala(7) = 0
        Else
            Escala(7) = Escala(7) + 1
        End If
        Escalar(Inst10Rango1, 2, Escala(7))
    End Sub
    Private Sub Instrumento11_Click(sender As Object, e As EventArgs) Handles Instrumento11.Click
        If Escala(8) = 2 Then
            Escala(8) = 0
        Else
            Escala(8) = Escala(8) + 1
        End If
        Escalar(Inst11Rango1, 3, Escala(8))
    End Sub
    Private Sub Instrumento12_Click(sender As Object, e As EventArgs) Handles Instrumento12.Click
        If Escala(9) = 2 Then
            Escala(9) = 0
        Else
            Escala(9) = Escala(9) + 1
        End If
        Escalar(Inst12Rango1, 1, Escala(9))
    End Sub
    Private Sub Instrumento13_Click(sender As Object, e As EventArgs) Handles Instrumento13.Click
        If Escala(10) = 2 Then
            Escala(10) = 0
        Else
            Escala(10) = Escala(10) + 1
        End If
        Escalar(Inst13Rango1, 2, Escala(10))
    End Sub
    Private Sub Instrumento14_Click(sender As Object, e As EventArgs) Handles Instrumento14.Click
        If Escala(11) = 2 Then
            Escala(11) = 0
        Else
            Escala(11) = Escala(11) + 1
        End If
        Escalar(Inst14Rango1, 3, Escala(11))
    End Sub
#End Region

    Private Sub btnAyuda_Click(sender As Object, e As EventArgs) Handles btnAyuda.Click
        AbrirAyuda()
    End Sub
    Public Sub AbrirAyuda()
        Dim pdf As Byte() = My.Resources.Ayuda
        Using tmp As New FileStream("Ayuda.pdf", FileMode.Create)
            tmp.Write(pdf, 0, pdf.Length)
        End Using
        Process.Start("Ayuda.pdf")
    End Sub


End Class
