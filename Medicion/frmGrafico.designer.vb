﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGrafico
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XyDiagram1 As DevExpress.XtraCharts.XYDiagram = New DevExpress.XtraCharts.XYDiagram()
        Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView1 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series2 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView2 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series3 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView3 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series4 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView4 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series5 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView5 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series6 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView6 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series7 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView7 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series8 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView8 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series9 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView9 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series10 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView10 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series11 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView11 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series12 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView12 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series13 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView13 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series14 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView14 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series15 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView15 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series16 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView16 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim Series17 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PointSeriesView17 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim PointSeriesView18 As DevExpress.XtraCharts.PointSeriesView = New DevExpress.XtraCharts.PointSeriesView()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGrafico))
        Me.InstrumentosTab = New System.Windows.Forms.Label()
        Me.TablaTab = New System.Windows.Forms.Label()
        Me.GuardarBtn = New System.Windows.Forms.Label()
        Me.ImprimirBtn = New System.Windows.Forms.Label()
        Me.GraficoCtrl = New DevExpress.XtraCharts.ChartControl()
        Me.CerrarPrincipal = New System.Windows.Forms.Label()
        Me.Pot4YBtn = New System.Windows.Forms.Button()
        Me.Acc2YBtn = New System.Windows.Forms.Button()
        Me.Vcc2YBtn = New System.Windows.Forms.Button()
        Me.Pot3YBtn = New System.Windows.Forms.Button()
        Me.Acc1YBtn = New System.Windows.Forms.Button()
        Me.Vcc1YBtn = New System.Windows.Forms.Button()
        Me.Cos2YBtn = New System.Windows.Forms.Button()
        Me.Pot2YBtn = New System.Windows.Forms.Button()
        Me.Aca2YBtn = New System.Windows.Forms.Button()
        Me.Vca2YBtn = New System.Windows.Forms.Button()
        Me.Pot1YBtn = New System.Windows.Forms.Button()
        Me.Vca1YBtn = New System.Windows.Forms.Button()
        Me.Pot4XBtn = New System.Windows.Forms.Button()
        Me.Acc2XBtn = New System.Windows.Forms.Button()
        Me.Vcc2XBtn = New System.Windows.Forms.Button()
        Me.Pot3XBtn = New System.Windows.Forms.Button()
        Me.Acc1XBtn = New System.Windows.Forms.Button()
        Me.Vcc1XBtn = New System.Windows.Forms.Button()
        Me.Cos2XBtn = New System.Windows.Forms.Button()
        Me.Pot2XBtn = New System.Windows.Forms.Button()
        Me.Aca2XBtn = New System.Windows.Forms.Button()
        Me.Vca2XBtn = New System.Windows.Forms.Button()
        Me.Pot1XBtn = New System.Windows.Forms.Button()
        Me.Aca1XBtn = New System.Windows.Forms.Button()
        Me.Vca1XBtn = New System.Windows.Forms.Button()
        Me.TBtn = New System.Windows.Forms.Button()
        Me.Cos1YBtn = New System.Windows.Forms.Button()
        Me.Cos1XBtn = New System.Windows.Forms.Button()
        Me.FHzYBtn = New System.Windows.Forms.Button()
        Me.FHzXBtn = New System.Windows.Forms.Button()
        Me.RPMYBtn = New System.Windows.Forms.Button()
        Me.RPMXBtn = New System.Windows.Forms.Button()
        Me.TqYBtn = New System.Windows.Forms.Button()
        Me.TqXBtn = New System.Windows.Forms.Button()
        Me.Aca1YBtn = New System.Windows.Forms.Button()
        Me.GrafChangebtn = New System.Windows.Forms.Button()
        CType(Me.GraficoCtrl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(XyDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PointSeriesView18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'InstrumentosTab
        '
        Me.InstrumentosTab.BackColor = System.Drawing.Color.Transparent
        Me.InstrumentosTab.Location = New System.Drawing.Point(104, 68)
        Me.InstrumentosTab.Name = "InstrumentosTab"
        Me.InstrumentosTab.Size = New System.Drawing.Size(149, 19)
        Me.InstrumentosTab.TabIndex = 49
        '
        'TablaTab
        '
        Me.TablaTab.BackColor = System.Drawing.Color.Transparent
        Me.TablaTab.Location = New System.Drawing.Point(259, 68)
        Me.TablaTab.Name = "TablaTab"
        Me.TablaTab.Size = New System.Drawing.Size(149, 19)
        Me.TablaTab.TabIndex = 50
        '
        'GuardarBtn
        '
        Me.GuardarBtn.BackColor = System.Drawing.Color.Transparent
        Me.GuardarBtn.Location = New System.Drawing.Point(125, 504)
        Me.GuardarBtn.Name = "GuardarBtn"
        Me.GuardarBtn.Size = New System.Drawing.Size(57, 19)
        Me.GuardarBtn.TabIndex = 78
        '
        'ImprimirBtn
        '
        Me.ImprimirBtn.BackColor = System.Drawing.Color.Transparent
        Me.ImprimirBtn.Location = New System.Drawing.Point(195, 504)
        Me.ImprimirBtn.Name = "ImprimirBtn"
        Me.ImprimirBtn.Size = New System.Drawing.Size(57, 19)
        Me.ImprimirBtn.TabIndex = 79
        '
        'GraficoCtrl
        '
        Me.GraficoCtrl.AppearanceNameSerializable = "Terracotta Pie"
        Me.GraficoCtrl.BackColor = System.Drawing.Color.Transparent
        Me.GraficoCtrl.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        XyDiagram1.AxisX.Color = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        XyDiagram1.AxisX.GridLines.Visible = True
        XyDiagram1.AxisX.Interlaced = True
        XyDiagram1.AxisX.InterlacedColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        XyDiagram1.AxisX.Tickmarks.CrossAxis = True
        XyDiagram1.AxisX.VisibleInPanesSerializable = "-1"
        XyDiagram1.AxisY.Interlaced = True
        XyDiagram1.AxisY.Tickmarks.CrossAxis = True
        XyDiagram1.AxisY.VisibleInPanesSerializable = "-1"
        XyDiagram1.DefaultPane.Shadow.Visible = True
        Me.GraficoCtrl.Diagram = XyDiagram1
        Me.GraficoCtrl.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Empty
        Me.GraficoCtrl.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        Me.GraficoCtrl.Location = New System.Drawing.Point(98, 180)
        Me.GraficoCtrl.Name = "GraficoCtrl"
        Me.GraficoCtrl.PaletteName = "Medicion"
        Me.GraficoCtrl.PaletteRepository.Add("Medicion", New DevExpress.XtraCharts.Palette("Medicion", DevExpress.XtraCharts.PaletteScaleMode.Repeat, New DevExpress.XtraCharts.PaletteEntry() {New DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(CType(CType(96, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(19, Byte), Integer)), System.Drawing.Color.FromArgb(CType(CType(96, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(19, Byte), Integer))), New DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(188, Byte), Integer)), System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(188, Byte), Integer))), New DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(45, Byte), Integer)), System.Drawing.Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(45, Byte), Integer))), New DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(30, Byte), Integer)), System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(30, Byte), Integer)))}))
        Series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series1.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series1.Name = "VCA1"
        PointSeriesView1.PointMarkerOptions.Size = 4
        Series1.View = PointSeriesView1
        Series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series2.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series2.Name = "ACA1"
        PointSeriesView2.PointMarkerOptions.Size = 4
        Series2.View = PointSeriesView2
        Series3.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series3.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series3.Name = "WCA1"
        PointSeriesView3.PointMarkerOptions.Size = 4
        Series3.View = PointSeriesView3
        Series4.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series4.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series4.Name = "COS1"
        PointSeriesView4.PointMarkerOptions.Size = 4
        Series4.View = PointSeriesView4
        Series5.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series5.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series5.Name = "VCA2"
        PointSeriesView5.PointMarkerOptions.Size = 4
        Series5.View = PointSeriesView5
        Series6.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series6.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series6.Name = "ACA2"
        PointSeriesView6.PointMarkerOptions.Size = 4
        Series6.View = PointSeriesView6
        Series7.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series7.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series7.Name = "WCA2"
        PointSeriesView7.PointMarkerOptions.Size = 4
        Series7.View = PointSeriesView7
        Series8.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series8.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series8.Name = "COS2"
        PointSeriesView8.PointMarkerOptions.Size = 4
        Series8.View = PointSeriesView8
        Series9.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series9.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series9.Name = "FHZ"
        PointSeriesView9.PointMarkerOptions.Size = 4
        Series9.View = PointSeriesView9
        Series10.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series10.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series10.Name = "VCC1"
        PointSeriesView10.PointMarkerOptions.Size = 4
        Series10.View = PointSeriesView10
        Series11.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series11.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series11.Name = "ACC1"
        PointSeriesView11.PointMarkerOptions.Size = 4
        Series11.View = PointSeriesView11
        Series12.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series12.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series12.Name = "WCC1"
        PointSeriesView12.PointMarkerOptions.Size = 4
        Series12.View = PointSeriesView12
        Series13.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series13.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series13.Name = "VCC2"
        PointSeriesView13.PointMarkerOptions.Size = 4
        Series13.View = PointSeriesView13
        Series14.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series14.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series14.Name = "ACC2"
        PointSeriesView14.PointMarkerOptions.Size = 4
        Series14.View = PointSeriesView14
        Series15.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series15.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series15.Name = "WCC2"
        PointSeriesView15.PointMarkerOptions.Size = 4
        Series15.View = PointSeriesView15
        Series16.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series16.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series16.Name = "RPM"
        PointSeriesView16.PointMarkerOptions.Size = 4
        Series16.View = PointSeriesView16
        Series17.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Series17.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[False]
        Series17.Name = "TQ"
        PointSeriesView17.PointMarkerOptions.Size = 4
        Series17.View = PointSeriesView17
        Me.GraficoCtrl.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1, Series2, Series3, Series4, Series5, Series6, Series7, Series8, Series9, Series10, Series11, Series12, Series13, Series14, Series15, Series16, Series17}
        Me.GraficoCtrl.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical
        Me.GraficoCtrl.SeriesTemplate.View = PointSeriesView18
        Me.GraficoCtrl.Size = New System.Drawing.Size(848, 261)
        Me.GraficoCtrl.TabIndex = 80
        '
        'CerrarPrincipal
        '
        Me.CerrarPrincipal.BackColor = System.Drawing.Color.Transparent
        Me.CerrarPrincipal.Location = New System.Drawing.Point(996, 3)
        Me.CerrarPrincipal.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CerrarPrincipal.Name = "CerrarPrincipal"
        Me.CerrarPrincipal.Size = New System.Drawing.Size(17, 15)
        Me.CerrarPrincipal.TabIndex = 81
        '
        'Pot4YBtn
        '
        Me.Pot4YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Pot4YBtn.BackgroundImage = CType(resources.GetObject("Pot4YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Pot4YBtn.FlatAppearance.BorderSize = 0
        Me.Pot4YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Pot4YBtn.Location = New System.Drawing.Point(767, 152)
        Me.Pot4YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Pot4YBtn.Name = "Pot4YBtn"
        Me.Pot4YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Pot4YBtn.TabIndex = 77
        Me.Pot4YBtn.UseVisualStyleBackColor = False
        '
        'Acc2YBtn
        '
        Me.Acc2YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Acc2YBtn.BackgroundImage = CType(resources.GetObject("Acc2YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Acc2YBtn.FlatAppearance.BorderSize = 0
        Me.Acc2YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Acc2YBtn.Location = New System.Drawing.Point(722, 152)
        Me.Acc2YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Acc2YBtn.Name = "Acc2YBtn"
        Me.Acc2YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Acc2YBtn.TabIndex = 76
        Me.Acc2YBtn.UseVisualStyleBackColor = False
        '
        'Vcc2YBtn
        '
        Me.Vcc2YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Vcc2YBtn.BackgroundImage = CType(resources.GetObject("Vcc2YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Vcc2YBtn.FlatAppearance.BorderSize = 0
        Me.Vcc2YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Vcc2YBtn.Location = New System.Drawing.Point(677, 152)
        Me.Vcc2YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Vcc2YBtn.Name = "Vcc2YBtn"
        Me.Vcc2YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Vcc2YBtn.TabIndex = 75
        Me.Vcc2YBtn.UseVisualStyleBackColor = False
        '
        'Pot3YBtn
        '
        Me.Pot3YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Pot3YBtn.BackgroundImage = CType(resources.GetObject("Pot3YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Pot3YBtn.FlatAppearance.BorderSize = 0
        Me.Pot3YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Pot3YBtn.Location = New System.Drawing.Point(632, 152)
        Me.Pot3YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Pot3YBtn.Name = "Pot3YBtn"
        Me.Pot3YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Pot3YBtn.TabIndex = 74
        Me.Pot3YBtn.UseVisualStyleBackColor = False
        '
        'Acc1YBtn
        '
        Me.Acc1YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Acc1YBtn.BackgroundImage = CType(resources.GetObject("Acc1YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Acc1YBtn.FlatAppearance.BorderSize = 0
        Me.Acc1YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Acc1YBtn.Location = New System.Drawing.Point(587, 152)
        Me.Acc1YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Acc1YBtn.Name = "Acc1YBtn"
        Me.Acc1YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Acc1YBtn.TabIndex = 73
        Me.Acc1YBtn.UseVisualStyleBackColor = False
        '
        'Vcc1YBtn
        '
        Me.Vcc1YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Vcc1YBtn.BackgroundImage = CType(resources.GetObject("Vcc1YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Vcc1YBtn.FlatAppearance.BorderSize = 0
        Me.Vcc1YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Vcc1YBtn.Location = New System.Drawing.Point(542, 152)
        Me.Vcc1YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Vcc1YBtn.Name = "Vcc1YBtn"
        Me.Vcc1YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Vcc1YBtn.TabIndex = 72
        Me.Vcc1YBtn.UseVisualStyleBackColor = False
        '
        'Cos2YBtn
        '
        Me.Cos2YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Cos2YBtn.BackgroundImage = CType(resources.GetObject("Cos2YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Cos2YBtn.FlatAppearance.BorderSize = 0
        Me.Cos2YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Cos2YBtn.Location = New System.Drawing.Point(497, 152)
        Me.Cos2YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Cos2YBtn.Name = "Cos2YBtn"
        Me.Cos2YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Cos2YBtn.TabIndex = 71
        Me.Cos2YBtn.UseVisualStyleBackColor = False
        '
        'Pot2YBtn
        '
        Me.Pot2YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Pot2YBtn.BackgroundImage = CType(resources.GetObject("Pot2YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Pot2YBtn.FlatAppearance.BorderSize = 0
        Me.Pot2YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Pot2YBtn.Location = New System.Drawing.Point(453, 152)
        Me.Pot2YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Pot2YBtn.Name = "Pot2YBtn"
        Me.Pot2YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Pot2YBtn.TabIndex = 70
        Me.Pot2YBtn.UseVisualStyleBackColor = False
        '
        'Aca2YBtn
        '
        Me.Aca2YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Aca2YBtn.BackgroundImage = CType(resources.GetObject("Aca2YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Aca2YBtn.FlatAppearance.BorderSize = 0
        Me.Aca2YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Aca2YBtn.Location = New System.Drawing.Point(409, 152)
        Me.Aca2YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Aca2YBtn.Name = "Aca2YBtn"
        Me.Aca2YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Aca2YBtn.TabIndex = 69
        Me.Aca2YBtn.UseVisualStyleBackColor = False
        '
        'Vca2YBtn
        '
        Me.Vca2YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Vca2YBtn.BackgroundImage = CType(resources.GetObject("Vca2YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Vca2YBtn.FlatAppearance.BorderSize = 0
        Me.Vca2YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Vca2YBtn.Location = New System.Drawing.Point(365, 152)
        Me.Vca2YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Vca2YBtn.Name = "Vca2YBtn"
        Me.Vca2YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Vca2YBtn.TabIndex = 68
        Me.Vca2YBtn.UseVisualStyleBackColor = False
        '
        'Pot1YBtn
        '
        Me.Pot1YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Pot1YBtn.BackgroundImage = CType(resources.GetObject("Pot1YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Pot1YBtn.FlatAppearance.BorderSize = 0
        Me.Pot1YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Pot1YBtn.Location = New System.Drawing.Point(277, 152)
        Me.Pot1YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Pot1YBtn.Name = "Pot1YBtn"
        Me.Pot1YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Pot1YBtn.TabIndex = 67
        Me.Pot1YBtn.UseVisualStyleBackColor = False
        '
        'Vca1YBtn
        '
        Me.Vca1YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Vca1YBtn.BackgroundImage = CType(resources.GetObject("Vca1YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Vca1YBtn.FlatAppearance.BorderSize = 0
        Me.Vca1YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Vca1YBtn.Location = New System.Drawing.Point(189, 152)
        Me.Vca1YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Vca1YBtn.Name = "Vca1YBtn"
        Me.Vca1YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Vca1YBtn.TabIndex = 65
        Me.Vca1YBtn.UseVisualStyleBackColor = False
        '
        'Pot4XBtn
        '
        Me.Pot4XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Pot4XBtn.BackgroundImage = CType(resources.GetObject("Pot4XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Pot4XBtn.FlatAppearance.BorderSize = 0
        Me.Pot4XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Pot4XBtn.Location = New System.Drawing.Point(744, 152)
        Me.Pot4XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Pot4XBtn.Name = "Pot4XBtn"
        Me.Pot4XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Pot4XBtn.TabIndex = 64
        Me.Pot4XBtn.UseVisualStyleBackColor = False
        '
        'Acc2XBtn
        '
        Me.Acc2XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Acc2XBtn.BackgroundImage = CType(resources.GetObject("Acc2XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Acc2XBtn.FlatAppearance.BorderSize = 0
        Me.Acc2XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Acc2XBtn.Location = New System.Drawing.Point(699, 152)
        Me.Acc2XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Acc2XBtn.Name = "Acc2XBtn"
        Me.Acc2XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Acc2XBtn.TabIndex = 63
        Me.Acc2XBtn.UseVisualStyleBackColor = False
        '
        'Vcc2XBtn
        '
        Me.Vcc2XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Vcc2XBtn.BackgroundImage = CType(resources.GetObject("Vcc2XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Vcc2XBtn.FlatAppearance.BorderSize = 0
        Me.Vcc2XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Vcc2XBtn.Location = New System.Drawing.Point(654, 152)
        Me.Vcc2XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Vcc2XBtn.Name = "Vcc2XBtn"
        Me.Vcc2XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Vcc2XBtn.TabIndex = 62
        Me.Vcc2XBtn.UseVisualStyleBackColor = False
        '
        'Pot3XBtn
        '
        Me.Pot3XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Pot3XBtn.BackgroundImage = CType(resources.GetObject("Pot3XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Pot3XBtn.FlatAppearance.BorderSize = 0
        Me.Pot3XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Pot3XBtn.Location = New System.Drawing.Point(609, 152)
        Me.Pot3XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Pot3XBtn.Name = "Pot3XBtn"
        Me.Pot3XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Pot3XBtn.TabIndex = 61
        Me.Pot3XBtn.UseVisualStyleBackColor = False
        '
        'Acc1XBtn
        '
        Me.Acc1XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Acc1XBtn.BackgroundImage = CType(resources.GetObject("Acc1XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Acc1XBtn.FlatAppearance.BorderSize = 0
        Me.Acc1XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Acc1XBtn.Location = New System.Drawing.Point(564, 152)
        Me.Acc1XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Acc1XBtn.Name = "Acc1XBtn"
        Me.Acc1XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Acc1XBtn.TabIndex = 60
        Me.Acc1XBtn.UseVisualStyleBackColor = False
        '
        'Vcc1XBtn
        '
        Me.Vcc1XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Vcc1XBtn.BackgroundImage = CType(resources.GetObject("Vcc1XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Vcc1XBtn.FlatAppearance.BorderSize = 0
        Me.Vcc1XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Vcc1XBtn.Location = New System.Drawing.Point(519, 152)
        Me.Vcc1XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Vcc1XBtn.Name = "Vcc1XBtn"
        Me.Vcc1XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Vcc1XBtn.TabIndex = 59
        Me.Vcc1XBtn.UseVisualStyleBackColor = False
        '
        'Cos2XBtn
        '
        Me.Cos2XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Cos2XBtn.BackgroundImage = CType(resources.GetObject("Cos2XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Cos2XBtn.FlatAppearance.BorderSize = 0
        Me.Cos2XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Cos2XBtn.Location = New System.Drawing.Point(475, 152)
        Me.Cos2XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Cos2XBtn.Name = "Cos2XBtn"
        Me.Cos2XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Cos2XBtn.TabIndex = 58
        Me.Cos2XBtn.UseVisualStyleBackColor = False
        '
        'Pot2XBtn
        '
        Me.Pot2XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Pot2XBtn.BackgroundImage = CType(resources.GetObject("Pot2XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Pot2XBtn.FlatAppearance.BorderSize = 0
        Me.Pot2XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Pot2XBtn.Location = New System.Drawing.Point(431, 152)
        Me.Pot2XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Pot2XBtn.Name = "Pot2XBtn"
        Me.Pot2XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Pot2XBtn.TabIndex = 57
        Me.Pot2XBtn.UseVisualStyleBackColor = False
        '
        'Aca2XBtn
        '
        Me.Aca2XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Aca2XBtn.BackgroundImage = CType(resources.GetObject("Aca2XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Aca2XBtn.FlatAppearance.BorderSize = 0
        Me.Aca2XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Aca2XBtn.Location = New System.Drawing.Point(387, 152)
        Me.Aca2XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Aca2XBtn.Name = "Aca2XBtn"
        Me.Aca2XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Aca2XBtn.TabIndex = 56
        Me.Aca2XBtn.UseVisualStyleBackColor = False
        '
        'Vca2XBtn
        '
        Me.Vca2XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Vca2XBtn.BackgroundImage = CType(resources.GetObject("Vca2XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Vca2XBtn.FlatAppearance.BorderSize = 0
        Me.Vca2XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Vca2XBtn.Location = New System.Drawing.Point(343, 152)
        Me.Vca2XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Vca2XBtn.Name = "Vca2XBtn"
        Me.Vca2XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Vca2XBtn.TabIndex = 55
        Me.Vca2XBtn.UseVisualStyleBackColor = False
        '
        'Pot1XBtn
        '
        Me.Pot1XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Pot1XBtn.BackgroundImage = CType(resources.GetObject("Pot1XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Pot1XBtn.FlatAppearance.BorderSize = 0
        Me.Pot1XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Pot1XBtn.Location = New System.Drawing.Point(255, 152)
        Me.Pot1XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Pot1XBtn.Name = "Pot1XBtn"
        Me.Pot1XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Pot1XBtn.TabIndex = 54
        Me.Pot1XBtn.UseVisualStyleBackColor = False
        '
        'Aca1XBtn
        '
        Me.Aca1XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Aca1XBtn.BackgroundImage = CType(resources.GetObject("Aca1XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Aca1XBtn.FlatAppearance.BorderSize = 0
        Me.Aca1XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Aca1XBtn.Location = New System.Drawing.Point(211, 152)
        Me.Aca1XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Aca1XBtn.Name = "Aca1XBtn"
        Me.Aca1XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Aca1XBtn.TabIndex = 53
        Me.Aca1XBtn.UseVisualStyleBackColor = False
        '
        'Vca1XBtn
        '
        Me.Vca1XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Vca1XBtn.BackgroundImage = CType(resources.GetObject("Vca1XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Vca1XBtn.FlatAppearance.BorderSize = 0
        Me.Vca1XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Vca1XBtn.Location = New System.Drawing.Point(167, 152)
        Me.Vca1XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Vca1XBtn.Name = "Vca1XBtn"
        Me.Vca1XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Vca1XBtn.TabIndex = 52
        Me.Vca1XBtn.UseVisualStyleBackColor = False
        '
        'TBtn
        '
        Me.TBtn.BackColor = System.Drawing.Color.Transparent
        Me.TBtn.BackgroundImage = CType(resources.GetObject("TBtn.BackgroundImage"), System.Drawing.Image)
        Me.TBtn.FlatAppearance.BorderSize = 0
        Me.TBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TBtn.Location = New System.Drawing.Point(123, 152)
        Me.TBtn.Name = "TBtn"
        Me.TBtn.Size = New System.Drawing.Size(20, 17)
        Me.TBtn.TabIndex = 51
        Me.TBtn.UseVisualStyleBackColor = False
        '
        'Cos1YBtn
        '
        Me.Cos1YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Cos1YBtn.BackgroundImage = CType(resources.GetObject("Cos1YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Cos1YBtn.FlatAppearance.BorderSize = 0
        Me.Cos1YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Cos1YBtn.Location = New System.Drawing.Point(321, 152)
        Me.Cos1YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Cos1YBtn.Name = "Cos1YBtn"
        Me.Cos1YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Cos1YBtn.TabIndex = 83
        Me.Cos1YBtn.UseVisualStyleBackColor = False
        '
        'Cos1XBtn
        '
        Me.Cos1XBtn.BackColor = System.Drawing.Color.Transparent
        Me.Cos1XBtn.BackgroundImage = CType(resources.GetObject("Cos1XBtn.BackgroundImage"), System.Drawing.Image)
        Me.Cos1XBtn.FlatAppearance.BorderSize = 0
        Me.Cos1XBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Cos1XBtn.Location = New System.Drawing.Point(299, 152)
        Me.Cos1XBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Cos1XBtn.Name = "Cos1XBtn"
        Me.Cos1XBtn.Size = New System.Drawing.Size(20, 17)
        Me.Cos1XBtn.TabIndex = 82
        Me.Cos1XBtn.UseVisualStyleBackColor = False
        '
        'FHzYBtn
        '
        Me.FHzYBtn.BackColor = System.Drawing.Color.Transparent
        Me.FHzYBtn.BackgroundImage = CType(resources.GetObject("FHzYBtn.BackgroundImage"), System.Drawing.Image)
        Me.FHzYBtn.FlatAppearance.BorderSize = 0
        Me.FHzYBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.FHzYBtn.Location = New System.Drawing.Point(812, 152)
        Me.FHzYBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.FHzYBtn.Name = "FHzYBtn"
        Me.FHzYBtn.Size = New System.Drawing.Size(20, 17)
        Me.FHzYBtn.TabIndex = 85
        Me.FHzYBtn.UseVisualStyleBackColor = False
        '
        'FHzXBtn
        '
        Me.FHzXBtn.BackColor = System.Drawing.Color.Transparent
        Me.FHzXBtn.BackgroundImage = CType(resources.GetObject("FHzXBtn.BackgroundImage"), System.Drawing.Image)
        Me.FHzXBtn.FlatAppearance.BorderSize = 0
        Me.FHzXBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.FHzXBtn.Location = New System.Drawing.Point(789, 152)
        Me.FHzXBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.FHzXBtn.Name = "FHzXBtn"
        Me.FHzXBtn.Size = New System.Drawing.Size(20, 17)
        Me.FHzXBtn.TabIndex = 84
        Me.FHzXBtn.UseVisualStyleBackColor = False
        '
        'RPMYBtn
        '
        Me.RPMYBtn.BackColor = System.Drawing.Color.Transparent
        Me.RPMYBtn.BackgroundImage = CType(resources.GetObject("RPMYBtn.BackgroundImage"), System.Drawing.Image)
        Me.RPMYBtn.FlatAppearance.BorderSize = 0
        Me.RPMYBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RPMYBtn.Location = New System.Drawing.Point(857, 152)
        Me.RPMYBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.RPMYBtn.Name = "RPMYBtn"
        Me.RPMYBtn.Size = New System.Drawing.Size(20, 17)
        Me.RPMYBtn.TabIndex = 87
        Me.RPMYBtn.UseVisualStyleBackColor = False
        '
        'RPMXBtn
        '
        Me.RPMXBtn.BackColor = System.Drawing.Color.Transparent
        Me.RPMXBtn.BackgroundImage = CType(resources.GetObject("RPMXBtn.BackgroundImage"), System.Drawing.Image)
        Me.RPMXBtn.FlatAppearance.BorderSize = 0
        Me.RPMXBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RPMXBtn.Location = New System.Drawing.Point(834, 152)
        Me.RPMXBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.RPMXBtn.Name = "RPMXBtn"
        Me.RPMXBtn.Size = New System.Drawing.Size(20, 17)
        Me.RPMXBtn.TabIndex = 86
        Me.RPMXBtn.UseVisualStyleBackColor = False
        '
        'TqYBtn
        '
        Me.TqYBtn.BackColor = System.Drawing.Color.Transparent
        Me.TqYBtn.BackgroundImage = CType(resources.GetObject("TqYBtn.BackgroundImage"), System.Drawing.Image)
        Me.TqYBtn.FlatAppearance.BorderSize = 0
        Me.TqYBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TqYBtn.Location = New System.Drawing.Point(902, 152)
        Me.TqYBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.TqYBtn.Name = "TqYBtn"
        Me.TqYBtn.Size = New System.Drawing.Size(20, 17)
        Me.TqYBtn.TabIndex = 89
        Me.TqYBtn.UseVisualStyleBackColor = False
        '
        'TqXBtn
        '
        Me.TqXBtn.BackColor = System.Drawing.Color.Transparent
        Me.TqXBtn.BackgroundImage = CType(resources.GetObject("TqXBtn.BackgroundImage"), System.Drawing.Image)
        Me.TqXBtn.FlatAppearance.BorderSize = 0
        Me.TqXBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TqXBtn.Location = New System.Drawing.Point(879, 152)
        Me.TqXBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.TqXBtn.Name = "TqXBtn"
        Me.TqXBtn.Size = New System.Drawing.Size(20, 17)
        Me.TqXBtn.TabIndex = 88
        Me.TqXBtn.UseVisualStyleBackColor = False
        '
        'Aca1YBtn
        '
        Me.Aca1YBtn.BackColor = System.Drawing.Color.Transparent
        Me.Aca1YBtn.BackgroundImage = CType(resources.GetObject("Aca1YBtn.BackgroundImage"), System.Drawing.Image)
        Me.Aca1YBtn.FlatAppearance.BorderSize = 0
        Me.Aca1YBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Aca1YBtn.Location = New System.Drawing.Point(233, 152)
        Me.Aca1YBtn.Margin = New System.Windows.Forms.Padding(1)
        Me.Aca1YBtn.Name = "Aca1YBtn"
        Me.Aca1YBtn.Size = New System.Drawing.Size(20, 17)
        Me.Aca1YBtn.TabIndex = 90
        Me.Aca1YBtn.UseVisualStyleBackColor = False
        '
        'GrafChangebtn
        '
        Me.GrafChangebtn.BackgroundImage = Global.Medicion.My.Resources.Resources.Puntos
        Me.GrafChangebtn.Location = New System.Drawing.Point(914, 504)
        Me.GrafChangebtn.Name = "GrafChangebtn"
        Me.GrafChangebtn.Size = New System.Drawing.Size(32, 32)
        Me.GrafChangebtn.TabIndex = 91
        Me.GrafChangebtn.UseVisualStyleBackColor = True
        '
        'frmGrafico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Medicion.My.Resources.Resources.base_gráfico_16
        Me.ClientSize = New System.Drawing.Size(1024, 560)
        Me.ControlBox = False
        Me.Controls.Add(Me.GrafChangebtn)
        Me.Controls.Add(Me.Aca1YBtn)
        Me.Controls.Add(Me.TqYBtn)
        Me.Controls.Add(Me.TqXBtn)
        Me.Controls.Add(Me.RPMYBtn)
        Me.Controls.Add(Me.RPMXBtn)
        Me.Controls.Add(Me.FHzYBtn)
        Me.Controls.Add(Me.FHzXBtn)
        Me.Controls.Add(Me.Cos1YBtn)
        Me.Controls.Add(Me.Cos1XBtn)
        Me.Controls.Add(Me.CerrarPrincipal)
        Me.Controls.Add(Me.GraficoCtrl)
        Me.Controls.Add(Me.ImprimirBtn)
        Me.Controls.Add(Me.GuardarBtn)
        Me.Controls.Add(Me.Pot4YBtn)
        Me.Controls.Add(Me.Acc2YBtn)
        Me.Controls.Add(Me.Vcc2YBtn)
        Me.Controls.Add(Me.Pot3YBtn)
        Me.Controls.Add(Me.Acc1YBtn)
        Me.Controls.Add(Me.Vcc1YBtn)
        Me.Controls.Add(Me.Cos2YBtn)
        Me.Controls.Add(Me.Pot2YBtn)
        Me.Controls.Add(Me.Aca2YBtn)
        Me.Controls.Add(Me.Vca2YBtn)
        Me.Controls.Add(Me.Pot1YBtn)
        Me.Controls.Add(Me.Vca1YBtn)
        Me.Controls.Add(Me.Pot4XBtn)
        Me.Controls.Add(Me.Acc2XBtn)
        Me.Controls.Add(Me.Vcc2XBtn)
        Me.Controls.Add(Me.Pot3XBtn)
        Me.Controls.Add(Me.Acc1XBtn)
        Me.Controls.Add(Me.Vcc1XBtn)
        Me.Controls.Add(Me.Cos2XBtn)
        Me.Controls.Add(Me.Pot2XBtn)
        Me.Controls.Add(Me.Aca2XBtn)
        Me.Controls.Add(Me.Vca2XBtn)
        Me.Controls.Add(Me.Pot1XBtn)
        Me.Controls.Add(Me.Aca1XBtn)
        Me.Controls.Add(Me.Vca1XBtn)
        Me.Controls.Add(Me.TBtn)
        Me.Controls.Add(Me.TablaTab)
        Me.Controls.Add(Me.InstrumentosTab)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmGrafico"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(XyDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PointSeriesView18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GraficoCtrl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents InstrumentosTab As System.Windows.Forms.Label
    Friend WithEvents TablaTab As System.Windows.Forms.Label
    Friend WithEvents TBtn As System.Windows.Forms.Button
    Friend WithEvents Vca1XBtn As System.Windows.Forms.Button
    Friend WithEvents Aca1XBtn As System.Windows.Forms.Button
    Friend WithEvents Vca2XBtn As System.Windows.Forms.Button
    Friend WithEvents Pot1XBtn As System.Windows.Forms.Button
    Friend WithEvents Pot2XBtn As System.Windows.Forms.Button
    Friend WithEvents Aca2XBtn As System.Windows.Forms.Button
    Friend WithEvents Vcc1XBtn As System.Windows.Forms.Button
    Friend WithEvents Cos2XBtn As System.Windows.Forms.Button
    Friend WithEvents Vcc2XBtn As System.Windows.Forms.Button
    Friend WithEvents Pot3XBtn As System.Windows.Forms.Button
    Friend WithEvents Acc1XBtn As System.Windows.Forms.Button
    Friend WithEvents Pot4XBtn As System.Windows.Forms.Button
    Friend WithEvents Acc2XBtn As System.Windows.Forms.Button
    Friend WithEvents Vca1YBtn As System.Windows.Forms.Button
    Friend WithEvents Pot1YBtn As System.Windows.Forms.Button
    Friend WithEvents Vca2YBtn As System.Windows.Forms.Button
    Friend WithEvents Cos2YBtn As System.Windows.Forms.Button
    Friend WithEvents Pot2YBtn As System.Windows.Forms.Button
    Friend WithEvents Aca2YBtn As System.Windows.Forms.Button
    Friend WithEvents Pot3YBtn As System.Windows.Forms.Button
    Friend WithEvents Acc1YBtn As System.Windows.Forms.Button
    Friend WithEvents Vcc1YBtn As System.Windows.Forms.Button
    Friend WithEvents Pot4YBtn As System.Windows.Forms.Button
    Friend WithEvents Acc2YBtn As System.Windows.Forms.Button
    Friend WithEvents Vcc2YBtn As System.Windows.Forms.Button
    Friend WithEvents GuardarBtn As System.Windows.Forms.Label
    Friend WithEvents ImprimirBtn As System.Windows.Forms.Label
    Friend WithEvents GraficoCtrl As DevExpress.XtraCharts.ChartControl
    Friend WithEvents CerrarPrincipal As System.Windows.Forms.Label
    Friend WithEvents Cos1YBtn As System.Windows.Forms.Button
    Friend WithEvents Cos1XBtn As System.Windows.Forms.Button
    Friend WithEvents FHzYBtn As System.Windows.Forms.Button
    Friend WithEvents FHzXBtn As System.Windows.Forms.Button
    Friend WithEvents RPMYBtn As System.Windows.Forms.Button
    Friend WithEvents RPMXBtn As System.Windows.Forms.Button
    Friend WithEvents TqYBtn As System.Windows.Forms.Button
    Friend WithEvents TqXBtn As System.Windows.Forms.Button
    Friend WithEvents Aca1YBtn As System.Windows.Forms.Button
    Friend WithEvents GrafChangebtn As System.Windows.Forms.Button
End Class
