﻿Imports System.Data.OleDb 'Importa libreria para trabajar con BD Access
Imports System.Data 'Importa libreria para manejo de datos

Imports System
Imports System.Configuration

Public Class dbTabla
    Dim CadenaConexion As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}\DatosMedicion.mdb", Application.StartupPath)

    Public Sub Delete()
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdArticulo, el resto no importa)
        'Política de borrado: Borrado lógico

        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection() With {.ConnectionString = CadenaConexion}
        UnaConexion.Open()

        'Crear un comando con una consulta SQL con paso de parametros en linea
        Dim UnComando As New OleDbCommand("DELETE * FROM TblDatos", UnaConexion) With {.CommandType = CommandType.Text}
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub InsertarRegistros(UnaMedicion As Medicion)
        'Recibe un objeto cliente como parámetro

        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection() With {.ConnectionString = CadenaConexion}
        UnaConexion.Open()

        'Crear un comando con una consulta SQL con paso de parametros en linea
        Dim UnComando As New OleDbCommand("INSERT INTO TblDatos(Tiempo, Vca1, Aca1, Vca2, Aca2, PotenciaCA1, PotenciaCA2, PotenciaCC1, PotenciaCC2, Frecuencia, CosenoCA1, CosenoCA2, Vcc1, Acc1, Vcc2, Acc2, RPM, Torque, Medicion) VALUES (@Tiempo, @Vca1, @Aca1, @Vca2, @Aca2, @PotenciaCA1, @PotenciaCA2, @PotenciaCC1, @PotenciaCC2, @Frecuencia, @CosenoCA1, @CosenoCA2, @Vcc1, @Acc1, @Vcc2, @Acc2, @RPM, @Torque, @Medicion)", UnaConexion)

        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@Tiempo", UnaMedicion.Tiempo)
        UnComando.Parameters.AddWithValue("@Vca1", UnaMedicion.Vca1)
        UnComando.Parameters.AddWithValue("@Aca1", UnaMedicion.Aca1)
        UnComando.Parameters.AddWithValue("@Vca2", UnaMedicion.Vca2)
        UnComando.Parameters.AddWithValue("@Aca2", UnaMedicion.Aca2)
        UnComando.Parameters.AddWithValue("@PotenciaCA1", UnaMedicion.PotenciaCA1)
        UnComando.Parameters.AddWithValue("@PotenciaCA2", UnaMedicion.PotenciaCA2)
        UnComando.Parameters.AddWithValue("@PotenciaCC1", UnaMedicion.PotenciaCC1)
        UnComando.Parameters.AddWithValue("@PotenciaCC1", UnaMedicion.PotenciaCC2)
        UnComando.Parameters.AddWithValue("@Frecuencia", UnaMedicion.Frecuencia)
        UnComando.Parameters.AddWithValue("@CosenoCA1", UnaMedicion.CosenoCA1)
        UnComando.Parameters.AddWithValue("@CosenoCA2", UnaMedicion.CosenoCA2)
        UnComando.Parameters.AddWithValue("@Vcc1", UnaMedicion.Vcc1)
        UnComando.Parameters.AddWithValue("@Acc1", UnaMedicion.Acc1)
        UnComando.Parameters.AddWithValue("@Vcc2", UnaMedicion.Vcc2)
        UnComando.Parameters.AddWithValue("@Acc2", UnaMedicion.Acc2)
        UnComando.Parameters.AddWithValue("@RPM", UnaMedicion.RPM)
        UnComando.Parameters.AddWithValue("@Torque", UnaMedicion.Torque)
        UnComando.Parameters.AddWithValue("@Medicion", UnaMedicion.Medicion)

        UnComando.CommandType = CommandType.Text
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Function SelectRegistros() As DataTable
        'Recibe un objeto cliente como parámetro

        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection() With {.ConnectionString = CadenaConexion}
        UnaConexion.Open()

        'Crear un comando con una consulta SQL con paso de parametros en linea
        Dim UnComando As New OleDbCommand("SELECT * FROM tblDatos", UnaConexion)
        UnComando.CommandType = CommandType.Text
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()

        'Llenar tabla con adaptador
        Dim UnAdaptador As New OleDbDataAdapter(UnComando)
        Dim UnaTabla As New DataTable
        UnAdaptador.Fill(UnaTabla)

        UnaConexion.Close()

        Return UnaTabla
    End Function
    Public Function CountRegistros() As Integer
        'Recibe un objeto cliente como parámetro

        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection() With {.ConnectionString = CadenaConexion}
        UnaConexion.Open()

        'Crear un comando con una consulta SQL con paso de parametros en linea
        Dim UnComando As New OleDbCommand("SELECT COUNT (*) FROM tblDatos", UnaConexion)
        UnComando.CommandType = CommandType.Text
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()

        'Llenar tabla con adaptador
        Dim UnAdaptador As New OleDbDataAdapter(UnComando)
        Dim UnaTabla As New DataTable
        UnAdaptador.Fill(UnaTabla)

        'Convierte un DataRow en un dato tipo integer
        CountRegistros = 0
        Try
            CountRegistros = DirectCast((UnaTabla.Rows(0)(0)), Integer)
        Catch
        End Try

        UnaConexion.Close()

        Return CountRegistros
    End Function
End Class
