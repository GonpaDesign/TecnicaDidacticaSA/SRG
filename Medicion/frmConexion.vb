﻿Public Class frmConexion
    Dim myPort As Array  'COM Ports detected on the system will be stored here

#Region "Eventos Formularios"
    Private Sub Conexion_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        cmbport.Items.Clear()
        'When our form loads, auto detect all serial ports in the system and populate the cmbPort Combo box.
        myPort = IO.Ports.SerialPort.GetPortNames() 'Get all com ports available
        Try
            For i = 0 To UBound(myPort)
                cmbport.Items.Add(myPort(i))
            Next
            cmbport.Text = cmbport.Items.Item(0)    'Set cmbPort text to the first COM port detected
        Catch ex As Exception
            MsgBox("No se encontraron Puertos Serie Disponibles, por favor conecte el Hardware y Presione el boton actualizar.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Error")
        End Try
    End Sub
#End Region

#Region "Botones"
    Private Sub ConectarBtn_Click(sender As System.Object, e As System.EventArgs) Handles ConectarBtn.Click
        Me.SetStyle(ControlStyles.DoubleBuffer, True)
        If ConectarBtn.Text = "Conectar" Then
            ConectarBtn.Text = "Desconectar"
            Conectar()
            frmPrincipal.BotonesInstState(True)
            frmPrincipal.EnviarDatos(9)
            frmPrincipal.TablaTab.Enabled = True
            frmPrincipal.TablaTab.BackColor = Color.Transparent
            Me.Close() 'or Me.Hide()
        Else
            If frmPrincipal.SerialPort1.IsOpen Then
                frmPrincipal.DesconectarPuerto()
            End If
            ConectarBtn.Text = "Conectar"
        End If
    End Sub
    Private Sub ActualizaBtn_Click(sender As System.Object, e As System.EventArgs) Handles ActualizaBtn.Click
        cmbport.Items.Clear()
        'When our form loads, auto detect all serial ports in the system and populate the cmbPort Combo box.
        myPort = IO.Ports.SerialPort.GetPortNames() 'Get all com ports available
        Try

            For i = 0 To UBound(myPort)
                cmbport.Items.Add(myPort(i))
            Next

            cmbport.Text = cmbport.Items.Item(0)    'Set cmbPort text to the first COM port detected

        Catch ex As Exception
            MsgBox("No se encontraron Puertos Serie Disponibles, por favor conecte el Hardware y Presione el boton actualizar.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Error")
        End Try
    End Sub
    Private Sub CerrarBtn_Click(sender As System.Object, e As System.EventArgs) Handles CerrarBtn.Click
        Me.Close() 'or Me.Hide()
    End Sub
#End Region

#Region "Funciones"
    Private Sub Conectar()
        frmPrincipal.SerialPort1.PortName = cmbport.Text         'Set SerialPort1 to the selected COM port at startup
        frmPrincipal.SerialPort1.BaudRate = 115200         'Set Baud rate to the selected value on 

        'Other Serial Port Property
        frmPrincipal.SerialPort1.Parity = IO.Ports.Parity.None
        frmPrincipal.SerialPort1.StopBits = IO.Ports.StopBits.One
        frmPrincipal.SerialPort1.DataBits = 8            'Open our serial port
        frmPrincipal.SerialPort1.Open()

        ' PONE A CERO A LOS INSTRUMENTOS
        Try
            frmPrincipal.SerialPort1.Write("0" & vbCr)
        Catch ex As Exception
            MsgBox("Error de Comunicacion, intente conectarse nuevamente con el puerto.", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "Error de Comunicacion")
            frmPrincipal.DesconectarPuerto()
        End Try
    End Sub
#End Region

End Class